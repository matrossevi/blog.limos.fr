Title: Conférence – table ronde : Monnaies virtuelles, Initial Coin Offerings (ICOs) et Blockchain : Innovations en finance majeures ?
Date: 2018-04-24 14:00
Category: <i class='fa fa-bullhorn' aria-hidden='true'></i> Blog

Dans le cadre de la Clermont Innovation Week, le Groupe ESC Clermont a organisé une conférence suivie d’une table ronde pour questionner les innovations en finance liées aux dernières évolutions du digital.

* La conférence introduira la soirée avec :

    * Xavier LAVAYSSIERE, CEO de la société ECAN, qui parlera des enjeux liés aux monnaies virtuelles, aux ICO et à la Blockchain.
    * Tristan COLOMBET, CEO de DOMRAIDER, qui permettra d’avoir un retour d’expérience sur une ICO réussie.

* La table ronde traitera des questions suivantes : « Les ICO sont-ils les IPO 2.0 de demain? » et « De l’ICO à la Blockchain : quelle(s) réalité(s) et quel modèle économique possible ? » avec les intervenants de ECAN et DOMRAIDER ainsi que :

    * Cécile THEBAULT, Directeur des participations chez SOFIMAC IM
    * Vincent MAZENOD, Ingénieur CNRS au LIMOS
    * Alexandre MONNIN et Fabrice BIEN, Enseignants-chercheurs au Groupe ESC Clermont
