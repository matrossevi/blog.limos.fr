Title: Sécurité Logicielle Web
Date: 2015-03-12 14:00
Category: <i class='fa fa-bullhorn' aria-hidden='true'></i> Blog

par Thomas LALLART (INRA - Avignon) et Vincent MAZENOD (CNRS - Clermont),

le jeudi 12 mars 2015 de 14h00 à 16h00, (Voir l'[affiche](http://audaces.asso.st/uploads/seminaires/affiche-SecuriteWeb-v2.pdf) ou la description sur [Indico](https://indico.in2p3.fr/event/11263/))

=> [Machine virtuelle de tests pour VirtualBox](http://audaces.asso.st/uploads/Lubuntu.vdi.zip) | Thomas Lallart.
