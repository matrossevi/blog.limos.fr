Title: Mindmaping et markdown
Date: 2017-09-20 10:10
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: Projets tutorés, www


## Contexte

Le [Mindmaping](http://www.mindmapping.com/fr/) ou carte mentale est une méthode créative et logique pour prendre des notes et consigner des idées, qui consiste littéralement à "cartographier" votre réflexion sur un thème.

Il existe de nombreux programmes et services permettant de réaliser des cartes mentales:

Ce projet vise à réaliser un service en ligne permettant de réaliser des cartes mentale grâce au langage markdown.

L'extension [markdown-mindmap](https://atom.io/packages/markdown-mindmap) pour l'éditeur [atom](https://atom.io/) est une implémentation approchant le résulat attendu.

![markdown-mindmap](https://i.github-camo.com/a1d969d7ee4c9bd6145f8abf35d99de1544276d4/68747470733a2f2f6769746875622e636f6d2f64756e64616c656b2f61746f6d2d6d61726b646f776e2d6d696e646d61702f626c6f622f6d61737465722f73637265656e73686f742e6769663f7261773d74727565)


Le composant javascript [markmap](https://github.com/dundalek/markmap) peut être une brique logicielle intéressante.

N.B. Ce composant ne permet pas de créer des branches de part et d'autres du sujet principal, et cette possibilité est à discuter.

Afin d'être utilisable la solution devra intégrer

* le partage des cartes mentales
* afficher le contenu texte (en dessous du titre) au survol de la survie
* possibilité d'affecter une url (cliquable) à un noeud
* valider l'ergonomie et l'affichage sur ordinateurs, tablettes et téléphone
* une gestion d'utilisateur avec inscription et récupération de mot de passe [optionnel]


## Résultat attendu

un dépôt sur [https://gitlab.isima.fr](https://gitlab.isima.fr) contenant

* le code source implémentant les fonctionnalités demandées
* un fichier ```Vagrantfile``` permettant de tester l'application via ```vagrant up```
* un fichier `README.md` documentant l'installation, la configuration et l'utilisation du service
* un rapport détaillé du travail réalisé

## Technologies

* [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), [JS](https://fr.wikipedia.org/wiki/JavaScript),  [Vagrant](https://www.vagrantup.com/), la technologie server-side et le SGBD sont à choisir.

## Points à considérer

* Choix des technologies
* ergonomie utilisateur

### Liens

* [FreeMind](https://fr.wikipedia.org/wiki/FreeMind)
* [framindmap.org](https://framindmap.org/mindmaps/index.html)
* [https://monod.lelab.tailordev.fr/](https://monod.lelab.tailordev.fr/) peut servir d'exemple pour l'ergonomie et le système de partage de document
    * présente notamment des idées pour l'ergonomie et le système de partage
