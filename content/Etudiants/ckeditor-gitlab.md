Title: CKEditor for gitlab
Date: 2017-09-20 10:55
Category: <i class='fa fa-graduation-cap' aria-hidden='true'></i> &Eacute;tudiants
Tags: Projets tutorés, www, Tor

## Contexte

[GitLab](https://about.gitlab.com/) est une forge logicielle.

Cette forge présente notamment une interface web riche en fonctionnalités.

Il est possible au niveau d'un dépôt d'éditer ou de créer un fichier texte, ou encore d'uploader un fichier PDF directement à partir de l'interface web.

Chaque action donne lieu à un commit sur le dépôt.

Ce projet vise à intégrer au mieux CKEditor à la forge [https://gitlab.isima.fr/](https://gitlab.isima.fr/) notamment dans le but d'éditer en [WYSIWYG](https://fr.wikipedia.org/wiki/What_you_see_is_what_you_get) la documentation utilisateur de l'ISIMA [doc.isima.fr](https://doc.isima.fr) qui utilise [http://www.mkdocs.org/](http://www.mkdocs.org/)

Dans une première phase les possibilités d'implémenter une telle fonctionnalité seront toute étuidié

Dans une seconde phase la fonctionnalité sera développé avec les technologies choisies dans la première phase?

## Points à considérer

* se documenter sur la mailleure façon d'implémenter les fonctionnalités
    * plugin gitlab?
    * plugin firefox / chrome?
    * service stand alone?
        * utilisation de la commande git?
        * utilisation de l'api gitlab?
    * app electron
* sécurité

## Technologies

* [GitLab](https://about.gitlab.com/), [git](https://fr.wikipedia.org/wiki/Git), ???

## Rendu attendu

* Vous devez rendre un dépot git avec au minimum
    * un code source implémentant les fonctionnalités demandées
    * un fichier `README.md` documentant l'installation, la configuration et l'utilisation du service
    * un rapport détaillé du travail réalisé
