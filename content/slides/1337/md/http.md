![alt text](images/http/http-slash-slash.png "http slash slash")


## client / serveur

![alt text](images/http/client-serveur.png "Client Serveur")


# HTTP

 * le serveur est typiquement un __serveur web__
 * le truc est typiquement une _ressource_ associée a une __URI__
 * le client est typiquement
   * un __navigateur web__
   * une __web app__
   * un __objet connecté__
   * __n'importe quoi__ qui consomme de l'API...


# HTTP

* inventé par [Tim Berners-Lee](http://fr.wikipedia.org/wiki/Tim_Berners-Lee) en 1989
* en [version 1.1](https://www.ietf.org/rfc/rfc2616.txt) depuis 1999
* version 2.0 en cours de standardisation
  * basée sur [SPDY](http://fr.wikipedia.org/wiki/SPDY) de Google

Note:
- périmètre de sécurité élargi browser / responsive / web app / API -> installé sur les mobiles
- Tim Berners Lee a également inventé les adresses web et le langage HTML qui forment le web
- URI terme le plus générique
- URN sans le protocole
- URL emplacement à suivre ne gère pas le déplacement de la ressource


## requête HTTP

 * une URI (Unified Resource Identifier)
 * quelques [en-têtes / headers](http://en.wikipedia.org/wiki/List_of_HTTP_header_fields)
    * contraintes sur le contenu demandé
    * informations contextuelles
    * sous la forme clé: valeur
 * un verbe HTTP décrivant l'action
 * une ligne vide
 * un corps servant à éventuellement envoyer des données

Note:
- dans la préhistoire le verbe est appelé méthode


## requête HTTP de la vraie vie

<pre><code data-trim>
GET / HTTP/1.1
Host:    perdu.com
User-Agent:  Mozilla/5.0 (X11; Linux i686; rv:35.0) Gecko/20100101 Firefox/35.0
Accept:  text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Connection:  keep-alive
If-Modified-Since:   Tue, 02 Mar 2010 18:52:21 GMT
If-None-Match:   "cc-480d5dd98a340"
Cache-Control:   max-age=0

useless data
</code></pre>

Note:
- Connection:  keep-alive multiplexage de requête - envoyer plusieurs requêtes HTTP via la même connexion TCP (SPDY utilise ça)
- Accept-Encoding: gzip, deflate commpressions supportées par le navigateur
- le reste du cache


## verbes HTTP

agissent sur une URI

 * __GET__: récupérer une _ressource_ ou une collection de _ressources_
 * __POST__: créer un nouvelle _ressource_
 * __PUT__: mettre à jour une _ressource_ existante ou créer une nouvelle _ressource_ à une URI donnée
 * __DELETE__: supprimer une _ressources_ données
 * __PATCH__: mettre à jour partiellement une _ressource_ existante


## verbes HTTP

* [Seuls GET et POST son implémentés dans les navigateurs](http://www.w3.org/TR/html5/forms.html#attr-fs-action)

* Must read: [Please do not patch like an idiot](http://williamdurand.fr/2014/02/14/please-do-not-patch-like-an-idiot/)

* c.f. REST plus loin dans cette présentation

Note:
- La liste n'est pas exhaustive: HEAD, OPTIONS, TRACE, CONNECT
- tous idempotents sauf POST
- PATCH n'est pas dans la spec


## réponse HTTP

composée

 * d'un code HTTP
 * de quelques [en-têtes / headers](http://en.wikipedia.org/wiki/List_of_HTTP_header_fields)
    * informations sur le contenu servi
    * sous la forme clé: valeur
 * du contenu envoyé


## réponse HTTP de la vraie vie

<pre>
<code data-trim>
HTTP/1.1 304 Not Modified
Date:    Fri, 20 Feb 2015 15:15:01 GMT
Server:  Apache
Connection:  Keep-Alive
Keep-Alive:  timeout=2, max=100
Etag:    "cc-480d5dd98a340"
Vary:    Accept-Encoding

&#x3C;html&#x3E;
    &#x3C;head&#x3E;&#x3C;title&#x3E;Vous Etes Perdu ?&#x3C;/title&#x3E;&#x3C;/head&#x3E;
    &#x3C;body&#x3E;
        &#x3C;h1&#x3E;Perdu sur l&#x27;Internet ?&#x3C;/h1&#x3E;
        &#x3C;h2&#x3E;Pas de panique, on va vous aider&#x3C;/h2&#x3E;
        &#x3C;strong&#x3E;* &#x3C;----- vous êtes ici &#x3C;/strong&#x3E;
    &#x3C;/body&#x3E;
&#x3C;/html&#x3E;
</code>
</pre>

Note:
- Notez le header server qui donne déjà de l'information utile
- Etag, vary concerne le cache


## les codes HTTP

 * __1xx__ Informational
 * __2xx__ Successful

     * __200__ OK
     * __201__ Created
     * __204__ No Content

 * __3xx__ Redirections

     * __301__ Moved Permanently
     * __302__ Found
     * __304__ Not Modified

Note:
- 201 typique aprè un post
- 204 signifie un body de requête vide
- 301 vs 302 on change d'URI ou pas (definitif temporaire)
- 304 aucun besoin de recharger le contenu body vide


## les codes d'erreur HTTP

 * __4xx__ Client Error

     * __400__ Bad Request
     * __401__ Unauthorized
     * __403__ Forbidden
     * __404__ Not Found
     * __405__ Method Not Allowed
     * __406__ Not Acceptable
     * __409__ Conflict
     * __415__ Unsupported Media Type
     * __418__ I’m a teapot


## les codes d'erreur HTTP

 * __5xx__ Server Error

     * __500__ Internal Server Error

liste complète sur [httpstatus.es](httpstatus.es)


## paramètres HTTP

deux types

 * ceux passés via la query string de l'URL
    * accessible via __$_GET__ en _PHP_
 * ceux passés via le corps de la requête
    * accessible via __$_POST__ en _PHP_
 * tous les paramètres sont disponibles dans __$_REQUEST__ en _PHP_


## REpresentational State Transfer

![alt text](images/http/richardson.png "Richardson Maturity Model")


## REpresentational State Transfer

Must read:
* [Haters gonna HATEOAS](http://timelessrepo.com/haters-gonna-hateoas).
* [L’architecture REST expliquée en 5 règles](https://blog.nicolashachet.com/niveaux/confirme/larchitecture-rest-expliquee-en-5-regles/)
* [Architectural Styles and
the Design of Network-based Software Architectures](https://www.ics.uci.edu/%7Efielding/pubs/dissertation/top.htm) by Roy Thomas Fielding 2000

Note:
- Level 0 - HTTP comme protocole de transfert uniquement (transport only). Style RPC (SOAP, XML-RPC) on pourrait utiliser autre chose comme protocole
- Level 1 - Structure en Ressources individuelles, notion d'id sur les objet
- Level 2 - le Client utilise les verbes HTTP & les serveur les codes HTTP
- Level 3 - Hypermedia Controls = Content Negotiation + HATEOAS
- Level 3 - Content Negotiation -> sélectionner le format de représentation pour une ressource (json, xml, etc ..) le serveur tente de répondre au mieux autant que faire se peut.
- Level 3 - HATEOAS -> découvarbilité de l'API l'app est vue comme une machine à états
- on est loin du site web au sens CMS, pourautant ces services sont soumis au mêmes vulnérabilités


## HTTP est "__stateless__"

![alt text](images/http/stateless.jpg "Stateless")

Note:
- Http est amnésique
- chaque requête est traitée comme une requête indépendante sans relation avec les précédentes


## les cookies

* introduits par Netscape en 1994 pour fiabiliser l'implémentation du panier d'achat virtuel
* envoyés sous forme d'en tête HTTP par le serveur
<pre><code class="http">Set-Cookie: name=value[; Max-Age=age][; expires=date]
    [; domain=domain_name][; path=some_path][; secure]
    [; HttpOnly]
</code></pre>
* renvoyés inchangés par le client à chaque requête
<pre><code class="http">Cookie: name=value</code></pre>


## les cookies

 * accessibles via __$_COOKIE__ en _PHP_
 * cloisonnés par domaine
   * accessibles via les sous domaines
    * blocable par l'option _domain_
   * [tracking cookie](../privacy/tracking.html)
     * êtes vous en [conformité avec la loi?](http://www.cnil.fr/vos-obligations/sites-web-cookies-et-autres-traceurs/que-dit-la-loi/)

Note:
- https://www.owasp.org/index.php/HttpOnly -> pas de manipulation client side ANTI-XSS
- https://www.owasp.org/index.php/SecureFlag -> accessible en https uniquement
- sous domaine, google maps / google play / google.com


## les sessions

 * données gérées côté serveur

    * transimission de l'ID de session uniquement

<pre><code data-trim>
    Cookie: PHPSESSID=hr0ms75gs6f7vlph0hhct2bjj3
</code></pre>

 * accessibles via __$_SESSION__ en _PHP_


## [__SOP__ Same Origin Policy](https://developer.mozilla.org/fr/docs/Web/JavaScript/)

  * concerne [XMLHttpRequest](https://fr.wikipedia.org/wiki/XMLHttpRequest) / [Ajax](https://fr.wikipedia.org/wiki/Ajax_(informatique%29)
  * restreint les interactions d'une app web aux ressources ayant la même origine
    * origine
      * le protocole
      * le port (si spécifié)
      * l'hôte


## [CORS](http://en.wikipedia.org/wiki/Cross-origin_resource_sharing)

  * Cross Origin Resource Sharing
  * contrôler les accès en mode cross-site
  * effectuer des transferts de données sécurisés

### client
<pre><code data-trim>
    Origin: http://www.foo.com
</code></pre>

### serveur
<pre><code data-trim>
    Access-Control-Allow-Origin: http://www.foo.com
</code></pre>


## [CORS](http://en.wikipedia.org/wiki/Cross-origin_resource_sharing)

* <span style="color:red">* </span> dans le cas d'une ressource 100% publique
* autorise tous les verbes HTTP
* remplace [JSONP](https://en.wikipedia.org/wiki/JSONP)
  * qui n'autorisait que la méthode GET

[MDN|Contrôle d'accès HTTP](https://developer.mozilla.org/fr/docs/HTTP/Access_control_CORS)

Note:
- l'introduction de cette nouvelle possibilité implique nécessairement que les serveurs doivent gérer de nouvelles entêtes, et doivent renvoyer les ressources avec de nouvelles entêtes également
- doit être supporté par le navigateur
- la valeur * est possible mais risquée
- requêtes simples, pré-vérifiées avec le verbe OPTIONS, avec habilitations en forcant l'envoie du cookie


## header, cookie, body, query string, script ...

![alt text](images/http/illuminati.jpg "Don't trust anyone")<!-- .element: width="35%" -->
