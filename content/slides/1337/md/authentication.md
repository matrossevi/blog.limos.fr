## authentification

"<i class="fa fa-wikipedia-w" aria-hidden="true"></i> processus permettant à un système de s'assurer de la légitimité de la demande d'accès faite par une entité (être humain ou un autre système...) afin d'autoriser l'accès de cette entité à des ressources du système (systèmes, réseaux, applications…) conformément au paramétrage du contrôle d'accès ."


## 3 concepts

* identification
* **authentification**
* ACL


## preuves

* ce que je sais
  * mot de passe, numéro d'identification personnel
* ce que je possède
  * carte d'identité, carte à puce, droit de propriété, certificat électronique, diplôme, passeport, Token, Token OTP, périphérique
* ce que je suis
  * photo, caractéristique physique, voire biométrie
* ce que je sais faire
  * geste, signature


# quelques techniques


## apache & .htaccess

* [EnablingUseOfApacheHtaccessFiles](https://help.ubuntu.com/community/EnablingUseOfApacheHtaccessFiles)
  * surcharge du vhost courant (déconseillé)

```
$ sudo vi /etc/apache2/apache2.conf

<Directory /var/www/>
  Options Indexes FollowSymLinks MultiViews
  AllowOverride All # None par défaut
  Order allow,deny
  allow from all
</Directory>
```

Note:
- potentiellement danegreux
  - php.ini surchargeable aussi
    - tems d'exec max
    - taille de fichier max
    - reporting d'erreur
      - configurable en cas de virtual hosting
        - à creuser


## auth basic .htaccess

```
$ vi /var/www/http-basic/.htaccess

AuthType Basic
AuthName "Big Basic Secret"
AuthUserFile /var/www/http-basic/.htpasswd
Require valid-user
```

[htpasswd](https://httpd.apache.org/docs/2.2/programs/htpasswd.html) outil de création des utilisateurs

```
$ htpasswd -c /var/www/http-basic/.htpasswd guest
$ htpasswd /var/www/http-basic/.htpasswd otheruser
```


## auth basic

le client demande un contenu (protégé)

```http
GET /http-basic/ HTTP/1.1
Host: go.od
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0 Iceweasel/31.8.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
```


## auth basic

le serveur répond que le contenu est protégé

```http
HTTP/1.1 401 Authorization Required
Date: Wed, 21 Oct 2015 13:26:29 GMT
Server: Apache/2.2.22 (Debian)
WWW-Authenticate: Basic realm="Big Basic Secret"
Content-Length: 472
Content-Type: text/html; charset=iso-8859-1

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>401 Authorization Required</title>
</head><body>
<h1>Authorization Required</h1>
</body>
```


## auth basic

![HTTP basic auth](images/authentication/http-basic-auth.png "HTTP basic auth")


## auth basic

```http
GET /http-basic/ HTTP/1.1
Host: go.od
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0 Iceweasel/31.8.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Pragma: no-cache
Cache-Control: no-cache
Authorization: Basic b3dhc3A6cGFzc3dvcmQ=
```

[base64](https://fr.wikipedia.org/wiki/Base64)(username:password)


## auth digest

* Même schéma que la version Basic excepeté
  * algorithme de hashage [MD5](https://fr.wikipedia.org/wiki/MD5) plutôt qu'un simple encodage [base64](https://fr.wikipedia.org/wiki/Base64)
  * ajout d'un [nonce](http://fr.wikipedia.org/wiki/Nonce_cryptographique), rattacher à la session de communication, pour éviter les [attaques "par rejeu"](https://fr.wikipedia.org/wiki/Attaque_par_rejeu)

```
$ sudo a2enmod auth_digest
$ sudo service apache2 reload
```


## auth digest

!["Basic Digest Auth"](images/authentication/http-digest-auth.png "Basic Digest Auth")


## auth digest .htaccess

```
$ vi /var/www/http-digest/.htaccess

AuthType Digest
AuthName "authdigest"
AuthDigestDomain "/"
AuthDigestProvider file
AuthUserFile "/var/www/http-digest/.htdigest"
Require valid-user
```

[htdigest](https://httpd.apache.org/docs/2.2/fr/programs/htdigest.html) outil de création des utilisateurs

```
htdigest -c /var/www/http-basic/.htdigest "authdigest" guest
htdigest /var/www/http-basic/.htdigest "authdigest" otheruser
```      


## auth digest

* le client demande un contenu (protégé)
* le serveur répond que le contenu est protégé

```http
HTTP/1.1 401 Unauthorized
WWW-Authenticate: Digest realm="OwaspSample",
  nonce="Ny8yLzIwMDIgMzoyNjoyNCBQTQ",
  opaque="0000000000000000", \
  stale=false,
  algorithm=MD5,
  qop="auth"
```


## auth basic

![HTTP digest auth](images/authentication/http-basic-auth.png "HTTP digest auth")


## auth digest

```http
GET /example/owasp/test.asmx HTTP/1.1
Accept: */*
Authorization:  Digest username="owasp",
  realm="OwaspSample",
  qop="auth",
  algorithm="MD5",
  uri="/example/owasp/test.asmx",
nonce="Ny8yLzIwMDIgMzoyNjoyNCBQTQ",
nc=00000001,
cnonce="c51b5139556f939768f770dab8e5277a",
opaque="0000000000000000",
response="2275a9ca7b2dadf252afc79923cd3823"
```

Note:
- haché plus réversible même si MD5 ca craint un peu
- le nonce evite le rejeu
  - le serveur connait les nonce qu'il a émis
    - il sait qu'ils ne sont valables qu'une fois
    - http://slideplayer.fr/slide/1622495/ -> slide 30
- le cnonce agit comme un salt
  - évite le cassage du mot de passe par dictionnaire
- toujours pas de lockout
- attaque man in the middle
  - demande de nonce au serveur et l'envoie au client et renvoie le resultat du cacul au serveur
- pas de blocage testable à l'infini


## authentification par formulaire

le plus courant

```
<form method="POST" action="login">
<input type="text" name="username">
<input type="password" name="password">
</form>
```


## authentification par formulaire

* le mot de passe est transmis en clair au serveur
  * https :)
* stocker les mots de passe en clair sur le serveur est un mauvaise idée
  * [Rockyou.com en 2010](http://commedansdubeurre.ch/?story=162-piratage-de-32-millions-de-comptes-utilisateurs-chez-rockyou-recours-collectif-depose)
* stocker les hashés est une bonne pratique
* saler les hashés est encore mieux


## oAuth

![oAuth](images/authentication/oAuth.png "oAuth")


## oAuth

| Version | SSL         | Token           | Technique                |
| ------- | ----------- | --------------- | ------------------------ |
|  1.0    | optionel    | sans expiration | mise en place compliquée |
|  2.0    | obligatoire | révocable       | simple et plus léger     |

* 1.0 incompatible avec 2.0
* <i class="fa fa-newspaper-o"></i> [How to Secure Your REST API using Proven Best Practices](https://stormpath.com/blog/secure-your-rest-api-right-way)

Note:
- authentification simple (instagram)
- souvent autorisations partielles attribuées à l'app
- le token évite de rejouer l'auth
- oAuth 1.0 en clair lire le token
- pas de consensus sur la version 1 ou 2 sur Internet
- confiance dans consumer (l'app qui demande l'autorisation)


## JWT

[![jwt auth](images/authentication/jwt-auth.png "jwt auth")<!-- .element style="width: 80%" -->](https://blog.ippon.fr/2017/10/12/preuve-dauthentification-avec-jwt/)


## force brute

* par dictionnaires
  * liste d'utilisateurs + list de mots de passe
    * optimisable avec de la probabilité, [des dictionnaires au hasard](https://dazzlepod.com/site_media/txt/passwords.txt), de l'ingénieurie sociale, du flaire etc ...
* par recherche
    * combinatoire
* hybride
  * décliner via des règles les propositions d'un dictionnaire
  * leetspeakation automatique
  * [John the Ripper](http://www.openwall.com/john/) permet de générer des mots de passes dérivant de parties du username


## [THC Hydra](http://www.thc.org/thc-hydra/)

* multi-protocole: IMAP, SMB, HTTP, VNC, MS-SQL MySQL, SMTP, SSH, VNC, Asterisk, ...
* [RTFM](https://github.com/vanhauser-thc/thc-hydra)
* [liste de usernames](https://raw.githubusercontent.com/maryrosecook/commonusernames/master/usernames.txt)
* [liste de mots de passe](https://wiki.skullsecurity.org/index.php?title=Passwords)

<pre>
<code data-trim class="hljs bash" style="font-size: 22px">
hydra 192.168.1.26 ssh2 -s 22 -P pass.txt -L users.txt -e nrs -t 10
</code>
</pre>

* attaque du protocole ssh sur le port 22
* 10 threads à la fois
* essaie toutes les combinaisons possibles entre les username de users.txt et les mots de passe de pass.txt
  * + mot de passe vide
  * + mot de passe = username
  * + mot de passe = username à l'envers
* [OWASP - Testing for Brute Force](https://www.owasp.org/index.php/Testing_for_Brute_Force_%28OWASP-AT-004%29)
* [Using Hydra to dictionary-attack web-based login forms](http://insidetrust.blogspot.fr/2011/08/using-hydra-to-dictionary-attack-web.html)
* voir aussi [medusa](http://foofus.net/goons/jmk/medusa/medusa.html)

Note:
- Burp Suite
- Patator


## attaques offline

* Dépend de la puissance de calcul
* Non furtive
* Reproductible
* [OPHCRACK (the time-memory-trade-off-cracker)](http://lasecwww.epfl.ch/~oechslin/projects/ophcrack/)
  * monde windows
  * fichiers SAM
  * fonctionne avec des [dictionnaires](http://ophcrack.sourceforge.net/tables.php)
  * <i class="fa fa-gift"></i> <a href="http://www.newbiecontest.org/index.php?page=epreuve&no=224">obtenir le pass d'un compte windows en moins de 10 minutes</a><!-- tips: le xp free suffit -->
  * [Password Cracking: Lesson 2](http://www.computersecuritystudent.com/SECURITY_TOOLS/PASSWORD_CRACKING/lesson2/)
* [John the Ripper](http://www.openwall.com/john/)
  * monde UNIX/BSD
  * fichier /etc/passwd + /etc/shadow
  * mangling rules: leet speak, custom ...

<pre>
<code data-trim>
unshadow /etc/passwd /etc/shadow > mypasswd
</code>
</pre>

* [Checking Password Complexity with John the Ripper](http://www.admin-magazine.com/Articles/John-the-Ripper)


## attaques par tables arc-en-ciel

* [Project RainbowCrack](http://www.antsight.com/zsl/rainbowcrack/)
* [CrackStation](https://crackstation.net/)
  * avec la signature de smithy dans la table user

<pre>
<code data-trim>
5f4dcc3b5aa765d61d8327deb882cf99
</code>
</pre>


## 2AF

https://www.google.com/landing/2step/


## Yubikey

[![Ubikey](images/passwords/yubikey.png)](https://www.yubico.com/)


## <i class="fa fa-medkit"></i> Se protéger

* bien choisir son système d'authentification
* bien implémenter son système d'authentification
  * [ESAPI (The OWASP Enterprise Security API)](https://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API)
  * libs & bundles éprouvés
* mettre un nombre d'essais maximum consécutifs
  * plugin anti brute force
    * wordpress
    * drupal
  * compléter par des restrictions par IP, voir par blocs d'IPs
* instaurer des règles de durcissement au moment du choix du mot de passe
  * pas contournable côté client ;)
* sensibilisez vos utilisateurs
  * [<i class="fa fa-desktop"></i> Se protéger avec de bons mots de passe](http://doc.m4z3.me/_/bpu/se_proteger_avec_de_bons_mots_de_passe.htm#/cover)
  * [<i class="fa fa-video-camera"></i> Se protéger avec de bons mots de passe](http://webtv.u-clermont1.fr/media-MEDIA150410174414391)

* mettre en place en deuxième facteur d'authentification
* utiliser la crypto asymétrique
