# ansible

![ansible](images/ansible.png "ansible")<!-- .element width="30%" -->


* task
  * register
  * debug
  * loop
  * set_fact
  * pre_task
* roles

* playbook
  * ligne de commande
  * --check --diff
  * verbosité
  * tags
* variable (see debug task)
  * hierarchie
  * from env ou Vault
  * acces dynamique
  * ansible-vault : utiliser vault
* plugin
  * callback
  * filter
  * lookup
* modules ansible utile
  * pexpect
* remote roles


### Récupérer les roles nécessaires

* mettre à jour `requirements.yml` avec les rôles nécessaires

* récupérer les rôles

```
ansible-galaxy install -f -r requirements.yml -p ansible/roles/public
```
