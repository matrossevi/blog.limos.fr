# vagrant

![vagrant](images/vagrant.png "vagrant")<!-- .element width="30%" -->

**By HashiCorp**


## vagrant

* création / configuration d'environnements virtuels

* écrit en ruby
* [<i class="fa fa-github" aria-hidden="true"></i> hashicorp/vagrant](https://github.com/hashicorp/vagrant)
  * [<i class="fa fa-gavel" aria-hidden="true"></i> The MIT License](https://github.com/hashicorp/vagrant/blob/master/LICENSE)

* outil en ligne de commande
  * se voit dans l'hyperviseur utilisé


## vagrant

* Il peut être considéré comme un wrapper d'hyperviseur
  * VirtualBox
  * libvirt
  * VMware
  * Amazon EC2
* supporte nativement docker depuis la 1.6


## Installation

pré-requis

```
$ sudo apt install virtualbox
```

via gem

```
$ sudo apt install rubygems-integration
$ sudo gem install vagrant
```

via apt

```
$ sudo apt install vagrant
```

afficher la version de vagrant

```
$ vagrant --version
```


## Initialisation du projet

```
$ mkdir project && cd project
$ vagrant init
```

génère un Vagrantfile

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "base"
end
```

Beaucoup de commentaires ... laisser vous guider


## Box

* Machines virtuelles préconfigurées (templates)
* [vagrant cloud](https://app.vagrantup.com/boxes/search)
* [vagrantbox.es](https://www.vagrantbox.es/)
* mis en cache
* nommage à la github "développeur/Box"

```
$ vagrant box add "ubuntu/xenial64"
$ vagrant box add "http://aka.ms/vagrant-win7-ie11"
$ vagrant box list
$ vagrant box remove "ubuntu/xenial64"
```
* [Creating a Base Box](https://www.vagrantup.com/docs/boxes/base.html)
  * [<i class="fa fa-github" aria-hidden="true"></i> veewee](https://github.com/jedi4ever/veewee)
  * [<i class="fa fa-github" aria-hidden="true"></i> How to Create a CentOS Vagrant Base Box](https://github.com/ckan/ckan/wiki/How-to-Create-a-CentOS-Vagrant-Base-Box)


## cycle de vie

```
$ vagrant init "ubuntu/bionic64"
$ vagrant up #--provider=virtualbox
$ vagrant ssh
$ vagrant halt
$ vagrant suspend
$ vagrant reload
$ vagrant destroy #--force
```


## points de montage

montage automatique de `.` dans `/vagrant`

```
$ vagrant ssh
$ ll /vagrant
total 60
drwxr-xr-x  1 vagrant vagrant  4096 Dec 14 19:46 ./
drwxr-xr-x 24 root    root     4096 Dec 14 19:47 ../
drwxr-xr-x  1 vagrant vagrant  4096 Dec 14 19:46 .vagrant/
-rw-r--r--  1 vagrant vagrant   155 Dec 14 19:49 Vagrantfile
-rw-------  1 vagrant vagrant 44198 Dec 14 19:47 ubuntu-bionic-18.04-cloudimg-console.log
```

autre possibilité à partir de `Vagrantfile`

```ruby
Vagrant.configure("2") do |config|
 ...
 config.vm.provision "file", source: "~/.gitconfig", destination: "~/.gitconfig"
 ...
end
```


## provisioning

via l'entrée standard

```ruby
config.vm.provision "shell", inline: <<-SHELL
   sudo apt install -y python openssh-server
   SHELL
```

via un script

```ruby
Vagrant.configure("2") do |config|
 config.vm.box = "hashicorp/precise32"
 config.vm.provision "shell", path: "script.sh"
end
```


## provisioning

via [ansible](ansible.html)

```ruby
Vagrant.configure("2") do |config|
 config.vm.provision "ansible" do |ansible|    
  ansible.playbook = "playbook.yml"
  ansible.host_key_checking = false
  ansible.playbook = "vault.yml"
  ansible.extra_vars = { is_vagrant: true }
  ansible.tags = ['initialize']
  ansible.skip_tags = ["vagrant_context"]
  ansible.inventory_path = "./my-inventory"
  ansible.raw_arguments  = ["--private-key=~/.ssh/id/id_rsa"]
  ansible.verbose = "vvv"
 end
end
```
### idéal pour tester les playbooks


## vagrant pour tester apache

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"
  config.vm.network "forwarded_port", guest: 80, host: 8000
  config.vm.provider "virtualbox" do |vb|
     vb.gui = false
     vb.memory = "4096"
  end
  config.vm.provision "shell", inline: <<-SHELL
     sudo apt install -y apache2
     SHELL
end
```


## conclusion

### laissez un petit Vagrantfile dans vos playbooks est toujours une bonne idée!
