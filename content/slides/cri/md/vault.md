# vault
**By HashiCorp**

![vault](images/vault.png "vault")<!-- .element width="30%" -->

[https://vault.isima.fr](https://vault.isima.fr)


# Secrets

Il existe deux étages deux secrets dans la stack

* les **secrets locaux**: sont gérés par la commande `ansible-vault` et peuvent être lus simplement avec la commande `avq` de la stack (accessible une fois l'environnement activé). Les **secrets locaux** sont chiffrés en local, mais le fait qu'ils ne soient pas versionnés rend ce chiffrement non indispensable.

* les **secrets globaux**: sont gérés par la commande `vault` de HashiCorp et peuvent être lus simplement avec la commande `hvq` de la stack (accessible une fois l'environnement activé). les secrets sont disponibles selon les permissions du jeton utilisés. les **secrets globaux** sont utilisés pour construire les **secrets locaux**

## Générer un secret

Un secret robuste en ligne de commande se génère avec la commande `openssl` comme suit

```
$ openssl rand -base64 256
```

écrire le secret dans un fichier

```
$ openssl rand -base64 256 > ~/.ansible_secrets/stack
```

Stocker le secret directement dans hashicorp vault

```
$ openssl rand -base64 256 @TODO
```

## Vault

```
vault login -method=ldap username=vimazeno # vault login token=<token>
vault kv put cri/clusters/ovh/duncan/proxmoxapi password=itsasecret
vault read cri/clusters/ovh/duncan/proxmoxapi
vault read cri/clusters/ovh/duncan/proxmoxapi -format=json
vault read cri/clusters/ovh/duncan/proxmoxapi -format=json | jq .data
vault read cri/clusters/ovh/duncan/proxmoxapi -format=json | jq .data.password
vault delete cri/clusters/ovh/duncan/proxmoxapi
```

l'authentification ldap créée un fichier dans ~/.vault-token contenant votre token utilisateur avec vos permissions associées

```
vault kv patch cri/clusters/ovh/duncan/proxmoxapi password="$(openssl rand -base64 25)"
```

**N.B.** patch met à jour l'entrée avec KV2 et écrase les autres avec KV1 (on a activé KV2)

##  Stocker un secret

Ce secret devrait être stocké dans https://vault.isima.fr

Avec la commande `vault` [@TODO vault install] dans le path adéquat.

Interroger la structure du vault avec la command `vault-tree` avant de choisir le path du secret semble une bonne idée [vault-tree]().

Ces consignes sont valables pour tous le secrets de ce tutoriel
