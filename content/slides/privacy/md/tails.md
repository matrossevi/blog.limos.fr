# Tails

# <i class="fa fa-user-secret" aria-hidden="true"></i>


## Enjeu de l'OS

* Un OS contient
  * tous vos fichiers personnels
  * toutes les archives vos communications, mail, chat, etc ...
  * certains fichiers temporaires ou cachés dont vous ignorez l'existence
    * tous vos mots de passe enregistrés


## Enjeu de l'OS

* Booté à partir d'un autre système, n'importe quel disque en clair (système ou non)
  * est lisible avec les permissions d'administrateur

* Une solution est de chiffrer (chiffrement symétique) son système
  * Pour booter à partir d'un autre système l'attaquant devra connaître le mot de passe pour lire le disque en clair


## Enjeu de l'OS

Solution de chiffrement par OS

* [<i class="fa fa-apple" aria-hidden="true"></i> FileVault](https://fr.wikipedia.org/wiki/FileVault)
* [<i class="fa fa-windows" aria-hidden="true"></i> BitLocker](https://fr.wikipedia.org/wiki/BitLocker_Drive_Encryption)
* [<i class="fa fa-linux" aria-hidden="true"></i> dm-crypt](https://fr.wikipedia.org/wiki/Dm-crypt)
* [<i class="fa fa-apple" aria-hidden="true"></i> <i class="fa fa-windows" aria-hidden="true"></i> <i class="fa fa-linux" aria-hidden="true"></i> <strike>TrueCrypt</strike> VeraCrypt](https://www.veracrypt.fr/en/Home.html)
* [<i class="fa fa-android" aria-hidden="true"></i> C’est le moment de chiffrer votre téléphone Android](https://korben.info/cest-moment-de-chiffrer-telephone-android.html)
* [<i class="fa fa-apple" aria-hidden="true"></i> Guide d'utilisation de chiffrer votre iPhone](https://ssd.eff.org/fr/module/guide-dutilisation-de-chiffrer-votre-iphone)


## Enjeu de l'OS

* Une autre solution est l'utilisation d'un système Live
  * bootable à partir d'un support amovible (CD / USB)
  * avantage
    * une fois le système éteint aucune trace n'est accessible sur la machine
  * inconvénient
    * on repart avec un système vierge à chaque redémarrage


## Enjeu de l'OS

* Il est très compliqué de savoir les opérations qu'effectue un système
  * surtout si le code source de l'OS n'est pas analysable

* Une solution est d'utiliser un système d'exploitation open source
  * mieux un système open source réputé préoccupé par la vie privée et la sécurité


## Tails

* OS **live** basé sur **Debian**
* 64 bits uniquement depuis 2017
* Objectifs
  * préserver la vie privée
  * préserver l'anonymat
* Moyen
  * réduire ses traces sur la machine hôte
  * réduire / chiffrer les traces laissées sur le réseau
* résout le problème de l'amnésie d'un OS live


## Historique

* Juin 2009
  * descendant de [Incognito](https://en.wikipedia.org/wiki/Incognito_(operating_system))
* Financé par
  * [Tor project](https://www.torproject.org/)
  * [Debian](www.debian.org)
  * [Mozilla](https://www.mozilla.org/fr/)
  * [Freedom of the PressFoundation](https://fr.wikipedia.org/wiki/Freedom_of_the_Press_Foundation)


## [XKeyscore](https://fr.wikipedia.org/wiki/XKeyscore)

* Règles ciblant les personnes
  * cherchant des informations sur Tails à l'aide d'un moteur de recherche
  * visiteurs du site officiel

« *un outil de communication recommandé par des extrémistes sur des forums extrémistes* »

[NSA](https://fr.wikipedia.org/wiki/National_Security_Agency)


## Création du système live

![VM Tails](images/tails/vm-tails.png)


![VM Tails](images/tails/vm-tails-2.png)


![VM Tails](images/tails/vm-tails-3.png)


![VM Tails](images/tails/vm-tails-4.png)


![VM Tails](images/tails/vm-tails-5.png)


## Création d'une partition chiffrée

![VM Tails](images/tails/vm-tails-6.png)


![VM Tails](images/tails/vm-tails-7.png)


![VM Tails](images/tails/vm-tails-8.png)


## Suite logicielle

![VM Tails](images/tails/vm-tails-9.png)


![VM Tails](images/tails/vm-tails-10.png)


## Lancer

![VM Tails](images/tails/vm-tails-11.png)


## Tails

* [Site officiel](https://tails.boum.org/about/index.fr.html)
* [Documentation](https://tails.boum.org/doc/index.fr.html)
* [Tails download](https://tails.boum.org/install/)
* [Installer des logiciels additionnels](https://tails.boum.org/doc/advanced_topics/additional_software/index.fr.html)
