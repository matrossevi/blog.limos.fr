# Mots de passe

# <i class="fa fa-user-secret" aria-hidden="true"></i>


## Les mots de passes

1. ça ne se prête pas
2. ça ne se laisse pas traîner à la vue de tous
3. ça ne s'utilise qu'une fois
4. si ça casse on remplace immédiatement
5. un peu d'originalité ne nuit pas
6. la taille compte
7. il y a une date de péremption
8. mieux vaut les avoir avec soi


## C'est une question d'hygiène!

![preservatif](images/passwords/preservatif-darvador.jpg)<!-- .element width="30%" -->

[CNIL / Authentification par mot de passe : les mesures de sécurité élémentaires](https://www.cnil.fr/fr/authentification-par-mot-de-passe-les-mesures-de-securite-elementaires)


## [<i class="fa fa-firefox" aria-hidden="true"></i> Firefox](https://www.mozilla.org/fr/firefox/new/)

* Stocke les mots de passes en clair
  * *Préférences > Sécurité > Identifiants enregistrés*

![/o\](images/passwords/password.firefox.png)<!-- .element style="width: 500px" -->

* Propose un "mot de passe principal"
  * *Préférences > Sécurité > Utiliser un mot de passe principal*
    * [Vulnérable](https://www.raymond.cc/blog/how-to-find-hidden-passwords-in-firefox/)


## [<i class="fa fa-chrome" aria-hidden="true"></i> Chrome](https://www.google.fr/chrome/browser/desktop/)

* Stocke les [mots de passe en ligne](https://passwords.google.com/settings/passwords)
  * non [souverain](sovereignty.html)
    * comme [LastPass](https://www.lastpass.com/fr), [Dashlane](https://www.dashlane.com/), [iCloud](https://www.icloud.com/), ...

![/o\](images/passwords/password.google.png)


## [KeePass](http://keepass.info/)

* Gestionnaire (base de données, wallet, ...) de mots de passe
    * **souverain**

* <i class="fa fa-bullhorn" aria-hidden="true"></i> [Produits Certifiés CSPN par l'ANSSI](https://www.ssi.gouv.fr/entreprise/certification_cspn/keepass-version-2-10-portable/)

* <i class="fa fa-bullhorn" aria-hidden="true"></i> reco CNRS à l'[ANF Protection des données par le chiffrement (2015)](http://resinfo.org/spip.php?article86)                
    * [<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
 Gérer ses mots de passe avec KeePass et KeeFox par M. Libes](http://cesar.resinfo.org/IMG/pdf/07-anf_chiffre_-_tp_keepass_-_procedure.pdf)


## [KeePass](http://keepass.info/)

<i class="fa fa-windows" aria-hidden="true"></i> Full windows

* intégration en [mono](http://www.mono-project.com/) sous linux & Mac OS X
  * bugué sous certains environnement linux
* [nombreux plugins](http://keepass.info/plugins.html)
  * dont [keepasshttp](https://github.com/pfn/keepasshttp) <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> qui permet l'intégration au navigateur


## [KeePass](http://keepass.info/)

* KeePass 1 (Classic Edition)
  * .kdb

* KeePass 2 <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> (Professional Edition)
  * .kdbx <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
    * meilleure interropérabilité
  * permet d'attacher des fichiers
  * permet de lier un utilisateur windows à une base de données    


## [KeePass](http://keepass.info/) / config

#### Tools > Options

![Keepass config](images/passwords/keepass.config.png)


### [KeePassX](https://www.keepassx.org/) <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>

* Portage officieux de keepass
  * cross-platform (<i class="fa fa-windows" aria-hidden="true"></i> Windows, <i class="fa fa-apple" aria-hidden="true"></i> Mac OS X, <i class="fa fa-linux" aria-hidden="true"></i> Linux)

* Développement chaotique
    * non intégration d'une partie du code produit par la communauté
        * notamment le portage du plugin [keepasshttp](http://keepass.info/plugins.html#keepasshttp)


## [KeePassXC](https://keepassxc.org/) <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>

* Portage officieux de keepass
    * [KeepassXC – A cross-platform community fork of KeepassX](https://news.ycombinator.com/item?id=13468261)
    * cross-platform (<i class="fa fa-windows" aria-hidden="true"></i> Windows, <i class="fa fa-apple" aria-hidden="true"></i> Mac OS X, <i class="fa fa-linux" aria-hidden="true"></i> Linux)

* <strike>Récemment</strike> parfaitement stable
  * la [pulse du projet](https://github.com/keepassxreboot/keepassxc/pulse)

* Implémente en natif <strike>le RPC via http</strike> Unix sockets (ou pipes sous Windows)  pour l'intégration aux navigateurs <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>


## [KeePassXC](https://keepassxc.org/) / config

#### Tools > Options

![KeepassXC config](images/passwords/keepassxc.config.general.png)


## [KeePassXC](https://keepassxc.org/) / config

#### Tools > Options

![KeepassXC config](images/passwords/keepassxc.config.http.png)


### [<i class="fa fa-firefox" aria-hidden="true"></i> <i class="fa fa-chrome" aria-hidden="true"></i> KeePassXC-Browser Migration](https://keepassxc.org/docs/keepassxc-browser-migration/)

* pré-requis
  * [KeePassXC](https://keepassxc.org/) avec l'intégration aux bon navigateurs activée
* cherche un couple login mot de passe dans [KeePassXC](https://keepassxc.org/) à partir de l'url consultée
* sauvegarde les nouvelles entrées dans [KeePassXC](https://keepassxc.org/)


## Android

* [KeePassDroid](http://www.keepassdroid.com/) [<i class="fa fa-github" aria-hidden="true"></i>](https://github.com/bpellin/keepassdroid)
    * disponible sur l'app store alternatif [F-Droid](https://f-droid.org/)
        * catalogue of FOSS (Free and Open Source Software) applications for the Android platform

* [KeePass2Android](https://play.google.com/store/apps/details?id=keepass2android.keepass2android&hl=fr)
    * basé sur [KeePassDroid](http://www.keepassdroid.com/)
        * pas de code source disponible
    * intégration à Android


## KeePass2Android / config

![android.1.keepas2android](images/passwords/android.1.keepas2android.png)<!-- .element width="35%" -->
![android.2.keepas2android](images/passwords/android.2.keepas2android.png)<!-- .element width="35%" -->


## KeePass2Android / config

![android.3.parametres](images/passwords/android.3.parametres.png)<!-- .element width="30%" -->
![android.4.langues](images/passwords/android.4.langues.png)<!-- .element width="30%" -->
![android.5.clavier.config](images/passwords/android.5.clavier.config.png)<!-- .element width="30%" -->


## KeePass2Android / config

![android.6.clavier.disable](images/passwords/android.6.clavier.disable.png)<!-- .element width="30%" -->
![android.7.clavier.warning](images/passwords/android.7.clavier.warning.png)<!-- .element width="30%" -->
![android.8.clavier.enabled](images/passwords/android.8.clavier.enabled.png)<!-- .element width="30%" -->


## KeePass2Android / config

![android.9.clavier](images/passwords/android.9.clavier.png)<!-- .element width="30%" -->
![android.10.clavier.selectionne](images/passwords/android.10.clavier.selectionne.png)<!-- .element width="30%" -->


## KeePass2Android / config

![android.11.accessibilite](images/passwords/android.11.accessibilite.png)<!-- .element width="30%" -->
![android.12.accessibilite.clavier.desactive](images/passwords/android.12.accessibilite.clavier.desactive.png)<!-- .element width="30%" -->
![android.13.accessibilite.clavier.desactive](images/passwords/android.13.accessibilite.clavier.desactive.png)<!-- .element width="30%" -->


## KeePass2Android / config

![android.14.accessibilite.clavier.warning](images/passwords/android.14.accessibilite.clavier.warning.png)<!-- .element width="30%" -->
![android.15.accessibilite.clavier.enbabled](images/passwords/android.15.accessibilite.clavier.enbabled.png)<!-- .element width="30%" -->


## KeePass2Android / config

![android.16.clavier.reglages](images/passwords/android.16.clavier.reglages.png)<!-- .element width="30%" -->
![android.17.clavier.params](images/passwords/android.17.clavier.params.png)<!-- .element width="30%" -->
![android.18.clavier.keepass2android](images/passwords/android.18.clavier.keepass2android.png)<!-- .element width="30%" -->


## iOS

* N'hésitez pas à m'envoyer vos pointeurs


## [Vault by HashiCorp](https://www.vaultproject.io/)


## Solution Hardware

[![mooltipass](images/passwords/mooltipass.jpg)](https://www.themooltipass.com/)
