# Tor

# <i class="fa fa-user-secret" aria-hidden="true"></i>


## Tor (The Onion Router)

* mis au point par l'armée américaine milieu des 90's
* le code est ouvert en 2004
* sponsorisé par l'[EFF](https://www.eff.org/fr)


## Darknet

* Tor est "un" [darknet](https://fr.wikipedia.org/wiki/Darknet) parmi d'autres
  * [Freenet](https://fr.wikipedia.org/wiki/Freenet), [I2P](https://fr.wikipedia.org/wiki/I2P), [GNUnet](https://fr.wikipedia.org/wiki/GNUnet), [RetroShare](https://fr.wikipedia.org/wiki/RetroShare), [ZeroNet](https://fr.wikipedia.org/wiki/ZeroNet)

* Chiffrement & Anonymisation
  * network overlay & mix network

* "Le" [darkweb](https://fr.wikipedia.org/wiki/Dark_web) est un des services du darknet Tor


## Anonymat

* N'anonymise pas complètement
  * réduit fortement la probabilité d'identification

* Utilisation
  * détectable par les sites /service consultés
    * [How to ban people from sites when using Tor](https://www.reddit.com/r/TOR/comments/7xsl7v/how_to_ban_people_from_sites_when_using_tor/)
  * détectable par le FAI


#### ++

* Réduit le tracking
* Dissidence politique
  * [Great Firewall of China](https://en.wikipedia.org/wiki/Great_Firewall)
* The Guardian, The New Yorker, ProPublica (hidden services & relais)
* Whistle blower (wikileaks)
* Activisme
* Informations / blogs / livres censurés
* Mail
* Forum
* Chat


#### --

* Extrémisme
* (Pédo) pornographie
* Market
  * armes
  * drugs
  * virus
  * leak
  * carte de crédit
  * identité
* Crime organisé


## Tor en stats

* [Tor metrics](https://metrics.torproject.org/)
* [Tor relays map](https://torflow.uncharted.software)
* [Tor est lent](https://svn.torproject.org/svn/projects/roadmaps/2009-03-11-performance.pdf)
  * [Mais ce peut être amélioré](https://www.torproject.org/docs/faq.html.en#WhySlow)
* [Tor utilise RSA 1024 etdevrait passer à Curve 25519 group / Ed25519](https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt)


## Tor Browser

* [Firefox ESR(Extended Support Release)](https://www.mozilla.org/en-US/firefox/organizations/)
  * 4 extensions
    * TorButton
    * TorLauncher
    * NoScript
    * HTTPS Everywhere
  * Tor proxy


## Tor Browser

* [cross platform](https://www.torproject.org/projects/torbrowser.html.en) <i class="fa fa-linux" aria-hidden="true"></i> <i class="fa fa-windows" aria-hidden="true"></i> <i class="fa fa-apple" aria-hidden="true"></i>
  * version portable <i class="fa fa-windows" aria-hidden="true"></i>
* mobile [<i class="fa fa-android" aria-hidden="true"></i>](https://play.google.com/store/apps/details?id=org.torproject.android&hl=fr) [<i class="fa fa-apple" aria-hidden="true"></i>](https://itunes.apple.com/fr/app/onion-browser-secure-anonymous/id519296448?mt=8&ign-mpt=uo%3D4) [Sailfish OS](https://openrepos.net/content/nieldk/tor)

* [Outbound Ports](https://www.torproject.org/docs/faq.html.en#OutboundPorts) 80, 443, 9001, 9030
  * fichier de configuration [torrc](https://www.torproject.org/docs/faq.html.en#torrc)
    * [option "FascistFirewall 1"](https://www.torproject.org/docs/faq.html.en#FirewallPorts)

### <i class="fa fa-exclamation-circle" aria-hidden="true"></i> quelle confiance avez vous en votre OS?


## Tor

* Protège toujours le ***client***
* Ne protège pas les serveurs sur le web de surface
  * trahi par la correspondance DNS / IP
* Les **hidden services** ou **services cachés** (***SC***)
  * sont stockés "dans" Tor de manière anonyme
  * mais ne sortent pas de Tor
    * extension spécifique **.onion**
      * consultable via Tor Browser


## Routage

* Tor fonctionne avec 3 types de relais
  * **relais d’entrée** (*RE*)
    * **relais Gardien** (*RG*)
    * **bridge** (*RB*)
  * **relais intermédiaire** (*RI*)
  * **relais de sortie** (*RS*)
    * **relais Hidden Service Directory** (*HSDir*)
      * annuaire de **services cachés** (***SC***)


## Relais

* [Les relais Tor par type (*Flag*)](https://metrics.torproject.org/relayflags.html?start=2017-10-25&end=2018-01-23&flag=Running&flag=Exit&flag=Fast&flag=Guard&flag=Stable&flag=HSDir)

* [Héberger des relais Tor](https://www.torproject.org/docs/tor-doc-relay.html.en)
* [The New Guide to Running a Tor Relay](https://blog.torproject.org/new-guide-running-tor-relay)


## Circuit Tor

```
***client*** > RE > RI > RS > serveur
```

* contrôler le *RE* et le *RS* d'un circuit
  * permet de désanonymiser ce circuit

* les circuits sont renouvelés toutes les 10 minutes
  * le *RE* lui ne change que tous les 2 à 3 mois
    * [Minimise la probabilité d'utiliser de manière cumulative des circuits compromis](https://www.freehaven.net/anonbib/cache/wpes12-cogs.pdf)


## Circuit Tor

![How tor works](https://i.stack.imgur.com/XhXQa.png)


## Circuit Tor

* seul le *RE* connaît Alice
  * le *RE* ne connait pas Bob
* seul le *RS* connaît Bob
  * le *RS* ne connaît pas Alice
  	* le *RS* voit le contenu de la requête d'Alice


## Relais d'entrée (*RE*)

* relais Gardien (*RG*)
  * liste d'IPs publiques
    * connues des FAI
      * bloquables

* relais bridges (*RB*)
  * liste d'IPs secrètes
    * [distribuer au compte gouttes](https://bridges.torproject.org/bridges)


#### Relais gardien (*RG*)

* [Relay Search flag:Guard](https://atlas.torproject.org/#search/flag:Guard)
* [Tor Node List](https://www.dan.me.uk/tornodes)
* [Tor Network Status](https://torstatus.blutmagie.de/)

* Cruciale
  * pour le bon fonctionnement
  * pour la sécurité des utilisateurs

### Qui décide de cette liste?


## Autorités d’annuaire (*AA*)

> *"maintained by super trusted we-know-you-and-have-had-many-beers-with-you Tor volunteers"*

* [Relay Search flag:Authority](https://atlas.torproject.org/#search/flag:authority)
  * IP codées en dures dans les ***clients*** Tor
    * votent toutes les heures pour trouver un consensus sur la liste des relais à diffuser


## Consensus

* les *relais* envoient leurs infos aux *AA*
* les *AA* compilent indépendamment ces informations et évaluent chaque *relais*
  * passage au **flag Guardian**, **flag BadExit** ...
    * critères
      * bande passante, uptime, sécurité ...
* les *AA* votent pour obtenir un consensus
  * une liste commune
* les *AA* publient le consensus


## Rappel sur le web de surface

* HTTP tout passe en claire
* HTTPS, les données et l'url de la requête sont chiffrées
  * l'IP du site consulté est traçable
  * la requête DNS du ***client*** trahit le nom de domaine


## Tor sur le web de surface

* Téléchargement du consensus
  * liste des IPs des *RE*
* Sélection d'un circuit Tor aléatoire
* Récupération des clés publiques de chaque relais du circuit
  * à partir d'un serveur de clé


#### Handshake avec le ***RE***
<small>
1. Le ***client*** envoie une demande de connexion au ***RE***
2. Le ***RE*** signe le handshake avec sa clé privée
3. Le ***client*** vérifie la signature du ***RE***
  * avec la clé publique de ***RE***
    * récupérée sur le serveur de clé
4. Négociation d'une clé de session *KE*
  * via Diffie Hellman
5. Etablissement d'un canal chiffré symétriquement avec le **RE**

</small>


#### Handshake avec le ***RI***
<small>
Toutes les communications entre le ***client*** et le ***RE*** sont chiffrées avec *KE*

1. Le ***client*** envoie une demande de connexion au ***RE*** à transmettre au ***RI***
2. Le ***RE*** déchiffre la demande de connexion et transmet au ***RI***
3. Le ***RI*** signe le handshake avec sa clé privée et transmet au ***RE***
4. Le ***RE*** transmet au ***client***
5. Le ***client*** déchiffre le message reçu avec *KE* et vérifie la signature du ***RI***
  * avec la clé publique du ***RI***
    * récupérée sur le serveur de clé
6. Négociation d'une clé de session *KI* avec le ***RI***
  * via Diffie Hellman en passant par le ***RE***
7. Etablissement d'un canal chiffré symétriquement avec le ***RI***
  * le ***RI*** ne connait pas l'IP du ***client***

</small>


## Handshake avec le ***RS***
<small>
Toutes les communications entre le ***client*** et le ***RE*** sont chiffrées avec *KE*<br>
Toutes les communications entre le ***client*** et le ***RI*** sont chiffrées avec *KI* et transitent par le ***RE***

1. Le ***client*** envoie une demande de connexion chiffrée 2 fois: avec *KE*, puis avec *KI* et l’envoie au ***RE***
2. Le ***RE*** déchiffre la première couche avec *KE* et transmet au ***RI***
3. Le ***RI*** déchiffre avec *KI* et transmet au ***RS***
4. Le ***RS*** le handshake avec sa clé privée et renvoie au ***RI***
5. Le ***RI*** transmet au ***RE***
6. Le ***RE*** transmet au ***client***
7. Le ***client*** déchiffre le message reçu avec *KE* puis *KI* et vérifie la signature du ***RS***
  * avec la clé publique du ***RS***
    * récupérée sur le serveur de clé
8. Négociation d'une clé de session *KS* avec le *RS**
  * via Diffie Hellman en passant par le ***RE***, puis par le ***RI***
9. Etablissement d'un canal chiffré symétriquement avec le ***RS***
  * le ***RS*** ne connait ni l'IP du ***client***, ni l'IP du ***NE***

</small>


#### Echange de données
<small>
* Le ***client*** peut envoyer une requête http en utilisant de circuit
* Ce n'est plus le ***client*** qui émet la requête DNS mais le ***RS***
* le ***RS*** transmet la requête au serveur
* le ***RS*** transmet ensuite la réponse au ***RI*** en chiffrant avec *KS*
* le ***RI*** transmet ensuite la réponse au ***RE*** en chiffrant avec *KI*
* le ***RE*** transmet ensuite la réponse au ***client*** en chiffrant avec *KE*
* le ***client*** déchiffre alors successivement avec *KE*, *KI* et *KS*

</small>


## Echange de données

[![Tor circuit](images/tor/tor-circuit.png)](https://tor.stackexchange.com/questions/1932/does-exit-node-encrypt-three-times-for-the-return-path-to-local-machine)


#### Echange de données via HTTPS

![Tor avec HTTPS](https://cloud.m4z3.me/index.php/s/rywwyShzrwgLbrA/download)


## Service Caché (SC)

* .onion
  * uniquement via un ***client*** Tor

* [Hébergé un Hidden Service sur Tor](https://benjamin.sonntag.fr/Tor-les-onion-le-darknet-a-votre-portee)

* Système d'adressage basé sur des clés de chiffrement
	* protège l'IP du serveur
  * protège le serveur du DDoS

[Trawling Tor Hidden Service – Mapping the DHT](https://donncha.is/2013/05/trawling-tor-hidden-services/)


## Déclaration d'un ***SC***

* Etablissement de quelques circuits Tor
  * les ***RS*** de ces circuits sont sollicités pour jouer le rôle de points d'introductions ***PI*** pour le ***SC***

***SC*** > ***RE*** > ***RI*** > ***RS*** (***PI***)


## Déclaration d'un ***SC***

[![HS Step1](images/tor/HS-Step1.png)](images/tor/HS-Step1.png)


## Déclaration d'un ***SC***

* le ***SC*** publie sur un ***HSDir*** un descripteur contenant
  * Les IP des ***PI*** sollicitées
  * La clé publique du ***SC***
  * La signature de ces deux informations
    * faite avec la clé privée du ***SC***

***SC*** > ***RE*** > ***RI*** > ***RS*** (***HSDir***)

  * les ***HSDir*** partagent une [table de hachage distribuée (DHT)](https://fr.wikipedia.org/wiki/Table_de_hachage_distribu%C3%A9e) de descripteurs de ***SC***


## Déclaration d'un ***SC***

[![HS Step2](images/tor/HS-Step2.png)](images/tor/HS-Step2.png)


## Connexion à un ***SC***

* le ***client*** télécharge le consensus
* récupère des IPs de ***HSDir***
* les ***client*** interroge les ***HSDir*** via des circuits Tor
  * ***client*** > ***RE*** > ***RI*** > ***RS*** (***HSDir***)
* récupère le descripteur du ***SC*** et vérifie la signature
* le ***client*** peut alors contacter les ***PI***


## Connexion à un ***SC***

[![HS Step3](images/tor/HS-Step3.png)](images/tor/HS-Step3.png)


## Connexion à un ***SC***

* le ***client*** crée un circuit Tor
  * le relais de sorti sera le point de rendez vous (***RdV***)
  * ***client*** > ***RE*** > ***RI*** > ***RS*** (***RdV***)
* le ***client*** communique un ***secret*** au ***RdV***
  * le ***secret*** permettra d'authentifier le ***SC***


## Connexion à un ***SC***

[![HS Step4](images/tor/HS-Step4.png)](images/tor/HS-Step4.png)


## Connexion à un ***SC***

* le ***client*** créée un circuit Tor vers un des ***PI***
  * ***client*** > ***RE*** > ***RI*** > ***RS*** (***PI***)
* le ***client*** transmet au ***SC*** via le ***PI***
  * l'IP du ***RdV***
  * la première partie d'un échange Diffie Hellmann
  * le ***secret*** communiqué précédemment au ***RdV***
  * le tout chiffré est avec la clé publique du ***SC***
  * ***client*** > ***RE<inf>c</inf>*** > ***RI<inf>c</inf>*** > ***RS<inf>c</inf>*** > ***PI<inf>sc</inf>*** > ***RI<inf>sc</inf>*** > ***RE<inf>sc</inf>*** > ***SC***


## Connexion à un ***SC***

[![HS Step5](images/tor/HS-Step5.png)](images/tor/HS-Step5.png)


## Connexion à un ***SC***

* les ***SC*** contact le ***RdV***
  * transmet le ***secret*** qu'il a reçu du ***client***
    * authentification du ***SC***: il possède la clé publique du descripteur
      * puisqu'il a déchiffré le ***secret***


## Connexion à un ***SC***

[![HS Step6](images/tor/HS-Step6.png)](images/tor/HS-Step6.png)


## Connexion à un ***SC***

* Finalisation de l'échange Diffie Helmann entre ***client*** et ***SC***
  * un secret de communication est établi

***client*** > ***RE<inf>c</inf>*** > ***RI<inf>c</inf>*** > ***RS<inf>c</inf>*** (***RdV***) > ***RS<inf>sc</inf>*** > ***RI<inf>sc</inf>*** > ***RE<inf>sc</inf>*** > ***SC***


## Connexion à un ***SC***

[![HS Step7](images/tor/HS-Step7.png)](images/tor/HS-Step7.png)


## Circuit Tor d'échange avec un ***SC***

[![HS Step8](images/tor/HS-Step8.png)](images/tor/HS-Step8.png)


#### Trouver des .onion

* pas de "Google"
* sur le web de surface
  * [TheHiddenWiki](https://thehiddenwiki.org/)s
  * [reddit](https://www.reddit.com/r/TOR/)
  * [Deep To Web](https://www.deepdotweb.com/)
* sur des ***SC***


#### Quelques .onion

* [wikileaks](http://wikilw7ayqhjl6zr.onion/)
* [facebook](facebookcorewwwi.onion)
* [new york time](https://open.nytimes.com/https-open-nytimes-com-the-new-york-times-as-a-tor-onion-service-e0d0b67b7482)
* [USFakeIDs](http://en35tuzqmn4lofbk.onion/)
* [Deep Web Radio](http://76qugh5bey5gum7l.onion/)
* [Have I Been Powned](https://haveibeenpwned.com/)

##### toujours consolider les hash onions
<i class="fa fa-exclamation-circle" aria-hidden="true"></i> surtout pour les darkmarkets


## Tor over VPN

* VPN over Tor?
* [Auto VPN](https://github.com/adtac/autovpn)
