## GAFAM / BATX

![GAFAM/BATX](images/jnarac/gafa/gafam_batx.png)<!-- .element style="width: 80%" -->


## GAFAM / BATX

### GAFAM

Google + Apple + Facebook + Amazon + Microsoft

### BATX

Baidu + Alibaba + Tencent + Xiaomi


### Différents modèles économiques

![ads](images/jnarac/gafa/ads.jpg "ads")<!-- .element: width="70%" -->

* [Shot de dopamine : ce que Facebook fait au cerveau de mon amie Emilie](https://www.nouvelobs.com/rue89/notre-epoque/20171222.OBS9715/shot-de-dopamine-ce-que-facebook-fait-au-cerveau-de-mon-amie-emilie.html) #neuromarketing


## pour un seul mantra ...

![alt text](images/jnarac/gafa/dontbeevil.jpg "don't be evil")<!-- .element: width="70%" -->

### *"make the world a better place*" ...

Note:
- pas de méchant
- que des bienfaiteurs de l'humanité
- ricains, tout est à vendre, tout est business


## For my business!

![Epubs stats](images/jnarac/gafa/epub-stats.png "Epubs stats!")<!-- .element: width="60%" -->

#### avec optimisation fiscale

Note:
- ça rapporte, et surtout ça croît à fond
- ce qu'on sait le mieux à moins de frais: c'est créer de l'information
- en faire générer par l'utilisateur, le consommateur et faire du business avec

## Si c'est gratuit
### c'est l'utilisateur le produit!

![Si c'est gratuit, c'est vous le produit!](images/jnarac/gafa/pigs-and-the-free-model.jpg "Si c'est gratuit, c'est vous le produit!")<!-- .element: style="width:40%;" -->


## Données personnel

![Cartographie des données personnelles](images/jnarac/gafa/carto_donnees_personnelles.png)<!-- .element: width="60%" -->

### Le nouveau pétrole


## Bons apôtres <i class="fa fa-facebook" aria-hidden="true"></i>

![Zucki](images/jnarac/gafa/Mark_Zuckerberg.jpg)<!-- .element style="float: right; margin-left: 50px" -->

<!-- "People have really gotten comfortable not only sharing more information and different kinds, but more openly and with more people. [...] That social norm is just something that has evolved over time." -->

"Les gens sont désormais à l'aise avec l'idée de partager plus d'informations différentes, de manière plus ouverte et avec plus d'internautes. [...] La norme sociale a évolué."

[Mark Zuckerberg, PDG de Facebook, USTREAM, 10 janvier 2010](http://www.lemonde.fr/technologies/article/2010/01/11/pour-le-fondateur-de-facebook-la-protection-de-la-vie-privee-n-est-plus-la-norme_1289944_651865.html)

Note:
- Il a acheté toutes les maisons avec vis a vis sur la sienne


## Bons apôtres <i class="fa fa-facebook" aria-hidden="true"></i>

![Zucki se cache?](images/jnarac/gafa/zuc_cam.png)<!-- .element style="float: right; margin-left: 50px" -->


## Bons apôtres <i class="fa fa-google" aria-hidden="true"></i>

![Schmidti](images/jnarac/gafa/Eric_Schmidt.jpeg)<!-- .element style="float: left; margin-right: 50px" -->

"If you have something that you don't want anyone to know, maybe you shouldn't be doing it in the first place."

"S'il y a quelque chose que vous faites et que personne ne doit savoir, peut-être devriez vous commencer par ne pas le faire."

[Eric Schmidt, PDG de Google, The Register, 7 décembre 2009](http://www.theregister.co.uk/2009/12/07/schmidt_on_privacy/)

Note:
- on parle Google Facebook mais on parle GAFA et les autres
    - Même si Tim Cook communique sur l'importance de la vie privèe
- Keep Calm les GAFA et l' Asic (Association des services Internet communautaires)
    - se préocuppent aussi de défendre de notre vie privée
        - surtout si l'oppresseur c'est l'état


## Ce que <i class="fa fa-google" aria-hidden="true"></i> & <i class="fa fa-facebook" aria-hidden="true"></i> savent de vous

* [Google Mon activité](https://myactivity.google.com/myactivity)
* [Google position history - 20 novembre 2014](https://maps.google.com/locationhistory/b/0)
* [Google passwords](https://passwords.google.com/)
* Google [analytics](https://www.google.com/intl/fr/analytics/), [AdSense](https://www.google.fr/adsense/start/), [AdWords](https://adwords.google.com)
* [facebook peut savoir ce que vous n'avez pas osé publier](http://www.numerama.com/magazine/32751-facebook-peut-savoir-ce-que-vous-n-avez-pas-ose-publier.html)
* [facebook stockerait des données sur les non membres](http://www.numerama.com/magazine/20237-facebook-stockerait-des-donnees-sur-les-non-membres.html)
* [hello facebook identifie les numéros inconnus](http://www.numerama.com/magazine/32889-avec-hello-facebook-identifie-les-numeros-inconnus.html)


## <i class="fa fa-ambulance" aria-hidden="true"></i> protéger sa vie privée

* Lire les CGU
  * [Terms of Service; Didn't Read - TOSDR](https://tosdr.org/)
* Utiliser des services souverains
  * Conforme au [RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees)
* Etre [souverain](sovereignty.html) sur les outils que l'on utilise
  * Faire la différence entre logiciel
    * Gratuit
    * Open Source
    * Libre <i class="fa fa-smile-o" aria-hidden="true"></i>


## <i class="fa fa-ambulance" aria-hidden="true"></i> protéger sa vie privée

* Comprendre les [tracking](tracking.html) cookie
  * [<i class="fa fa-github" aria-hidden="true"></i> willdurand-edu/cookie-playground](https://github.com/willdurand-edu/cookie-playground)
* Eviter les tracking cookie
  * [<i class="fa fa-firefox" aria-hidden="true"></i> Ghostery](https://www.ghostery.com/fr/)
  * [<i class="fa fa-firefox" aria-hidden="true"></i> NoScript](https://noscript.net/)
* Utiliser Tor


## Ce que <i class="fa fa-google" aria-hidden="true"></i> & <i class="fa fa-facebook" aria-hidden="true"></i> changent

* [Deep Face - reconnaissance faciale](https://research.facebook.com/publications/480567225376225/deepface-closing-the-gap-to-human-level-performance-in-face-verification/)
* [Picasa](http://www.google.com/intl/fr/picasa/) et la reconnaissance automatique notamment des gens dans le temps
* [Êtes-vous déprimés ? Demandez à Google](https://www.sciencesetavenir.fr/sante/etes-vous-deprimes-demandez-a-google_115784)
* [Facebook détecte notre classe sociale. Et déclenche la lutte (algorithmique) finale](http://affordance.typepad.com/mon_weblog/2018/02/cest-la-lutte-algorithmique-finale-.html)
  * [Patent  "Socioeconomic group classification based on user features"](http://pdfaiw.uspto.gov/.aiw?PageNum=0&docid=20180032883&IDKey=175CD64C2991&HomeUrl=http%3A%2F%2Fappft.uspto.gov%2Fnetacgi%2Fnph-Parser%3FSect1%3DPTO1%2526Sect2%3DHITOFF%2526d%3DPG01%2526p%3D1%2526u%3D%25252Fnetahtml%25252FPTO%25252Fsrchnum.html%2526r%3D1%2526f%3DG%2526l%3D50%2526s1%3D%25252220180032883%252522.PGNR.%2526OS%3DDN%2F20180032883%2526RS%3DDN%2F20180032883)


## la société post vérité

[![RSA true](images/jnarac/www/rsa2.png)<!-- .element: width="45%" style="float: right" -->](http://rue89.nouvelobs.com/rue89-eco/2013/03/12/la-fable-bidon-de-la-famille-rsa-qui-gagne-plus-que-la-famille-salariee-240493)

[![RSA false](images/jnarac/www/rsa1.png)<!-- .element: width="45%" -->](http://rue89.nouvelobs.com/rue89-eco/2013/03/12/la-fable-bidon-de-la-famille-rsa-qui-gagne-plus-que-la-famille-salariee-240493)

[<small><i class="fa fa-newspaper-o" aria-hidden="true"></i> Quand on demande à Google si l’Holocauste a bien eu lieu...</small>](http://tempsreel.nouvelobs.com/rue89/rue89-sur-les-reseaux/20161226.RUE6067/quand-on-demande-a-google-si-l-holocauste-a-bien-eu-lieu.html)

[<small><i class="fa fa-newspaper-o" aria-hidden="true"></i> La tyrannie des agissants</small>](https://resistanceauthentique.net/tag/tyrannie-des-agissants/)


## <i class="fa fa-ambulance" aria-hidden="true"></i> Fakenews

### Fact-checking

* Identifier les __sources__
  * sites de la [fachosphère](https://fr.wikipedia.org/wiki/Extr%C3%AAme_droite_sur_Internet_et_Usenet)
  * sites [complotistes](http://rue89.nouvelobs.com/2016/01/01/charlie-hebdo-sont-sites-parlent-complot-257284)
  * sites à ligne éditoriale orientée
    * [RT en français](https://francais.rt.com/) (c.f. [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> RT](https://fr.wikipedia.org/wiki/RT_(cha%C3%AEne_de_t%C3%A9l%C3%A9vision))
  * sites lobbyistes (environnement, santé, sécurité)
    * <i class="fa fa-wikipedia-w" aria-hidden="true"></i>ikipédia?


## <i class="fa fa-ambulance" aria-hidden="true"></i> Fakenews

### avoir des repères

* [hoaxbuster](http://www.hoaxbuster.com/)
* [les décodeurs](http://www.lemonde.fr/les-decodeurs/)
  * [Décodex](http://www.lemonde.fr/verification/)
* [ontemanipule.fr](http://www.gouvernement.fr/on-te-manipule)
* [reddit <i class="fa fa-reddit" aria-hidden="true"></i>](https://www.reddit.com/)
* [Comment mieux repérer de fausses photos et vidéos](http://www.liberation.fr/desintox/2016/03/22/comment-mieux-reperer-de-fausses-photos-et-videos_1441248)
* [<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Les fake news n'existent pas (et c'est vrai)](http://www.bortzmeyer.org/fake-news.html)


## <i class="fa fa-ambulance" aria-hidden="true"></i> Fakenews

### un exemple

* [<i class="fa fa-newspaper-o" aria-hidden="true"></i> RAS-LE-BOL ! MICHEL SARDOU à HOLLANDE : « L’ISLAM est peut-être SUPER…Mais ON N’EN VEUT PAS !…»](http://lemondealenversblog.com/2015/07/26/ras-le-bol-michel-sardou-a-hollande-lislam-est-peut-etre-super-mais-on-nen-veut-pas/)
  * [<i class="fa fa-newspaper-o" aria-hidden="true"></i> Fausse lettre xénophobe : Michel Sardou est "abasourdi, effondré, sur le cul"](http://www.franceinfo.fr/emission/le-vrai-du-faux-numerique/2014-2015/michel-sardou-se-dit-impuissant-face-une-fausse-lettre-xenophobe-06-01-2015-17-52)
  * [<i class="fa fa-newspaper-o" aria-hidden="true"></i> PROUESSE ! BELGIQUE : elle se fait GREFFER UNE NOUVELLE OREILLE… cultivée dans son BRAS](http://lemondealenversblog.com/2015/06/19/prouesse-belgique-elle-se-fait-greffer-une-nouvelle-oreille-cultivee-dans-son-bras/#more-10347)

#### viralité = émotion + instantanéité

Note:
- centraliser la validation de la vérité sur 8 médictatures
    - est ce crédible


## <i class="fa fa-ambulance" aria-hidden="true"></i> Fakenews

### Tous les détails comptent!

<a href="http://faketrumptweet.com/fake-tweet/enb6lb5uuf_1hlzezi.png"><img src="http://i.faketrumptweet.com/enb6lb5uuf_1hlzezi.png" title="Made in America at faketrumptweet.com"/></a>

tapoter sept fois sur la coque de son smartphone avant de partager


## perspectives

![perspectives](images/jnarac/gafa/futurology.png)


## [Digital labor](https://fr.wikipedia.org/wiki/Travail_num%C3%A9rique)

* [« Sur Internet, nous travaillons tous, et la pénibilité de ce travail est invisible »](http://www.lemonde.fr/pixels/article/2017/03/11/sur-internet-nous-travaillons-tous-et-la-penibilite-de-ce-travail-est-invisible_5093124_4408996.html)
* [Faut pas prendre les usagers des gafa pour des datas sauvages](http://affordance.typepad.com//mon_weblog/2018/01/faut-pas-prendre-les-usagers-des-gafa-pour-des-datas-sauvages-.html)


## [Amazon Mechanical Turk](https://www.mturk.com/)

[![Mechanical turk](images/jnarac/gafa/The_turk.jpg)<!-- .element style="width:30%;" -->](https://en.wikipedia.org/wiki/The_Turk)

["A la rencontre des raters petites mains des Big Data"](thttps://theconversation.com/a-la-rencontre-des-raters-petites-mains-des-big-data-86484)


## Intelligence artificielle

![ReCaptcha](images/jnarac/gafa/reCaptcha.png)<!-- .element style="width:20%" -->

* ["Nous sommes les idiots utiles de GAFA et des BATX"](http://blog.barbayellow.com/2017/01/28/ia-education-et-revenu-universel/)

* [MIT Moral Machine](http://moralmachine.mit.edu/)


## IoT

* [Kinect pour Xbox One : un espion sans pareil dans votre salon ?](https://www.developpez.com/actu/56258/Kinect-pour-Xbox-One-un-espion-sans-pareil-dans-votre-salon-Non-assure-Microsoft-qui-rappelle-que-la-console-peut-etre-entierement-eteinte/)
* [Fitbit dévoile l'activité sexuelle de ses utilisateurs sur le Net](http://www.01net.com/actualites/une-appli-devoile-l-activite-sexuelle-de-ses-utilisateurs-sur-le-net-535193.html)
* [À 6 ans, elle discute avec Echo d'Amazon et commande une maison de poupée et des cookies](http://www.slate.fr/story/133601/amazon-echo-fille-commande)


## Transhumanisme

* [NBIC](https://fr.wikipedia.org/wiki/Nanotechnologies,_biotechnologies,_informatique_et_sciences_cognitives)
  * Nanotechnologies
  * Biotechnologies
  * Informatique
  * Sciences cognitives
* [Libertarianisme](https://fr.wikipedia.org/wiki/Libertarianisme)
* [Seasteading Institue](https://www.seasteading.org/)
* [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> Raymond Kurzweil](https://fr.wikipedia.org/wiki/Raymond_Kurzweil)
    *  directeur de l'ingénierie chez Google depuis 2012
