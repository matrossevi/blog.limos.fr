## Surveillance

!["surveillance"](images/jnarac/surveillance/obama-prism-raccroche.jpg "surveillance") <!-- .element: width="70%" -->

Note:
- Barack
- snowden juin 2013
- NSA
- Prism
  - grand retour en arrière au moins en facade
    - bad for business


## Tunisie (2011)

![Printemps Arabe](images/jnarac/surveillance/arab_spring.jpg)<!-- .element width="75%" -->

[<small>Tunisie : le principal fournisseur d'accès à Internet accusé de vol de mots de passe</small>](http://www.lemonde.fr/technologies/article/2011/01/07/l-agence-tunisienne-d-internet-accusee-de-vol-de-mots-de-passe_1462581_651865.html#OYeqItC9BKrqPzL5.9)


## Chine

![Chines cop](images/jnarac/surveillance/chinese_cop.jpg)<!-- .element width="85%" -->

[<small>Face Recognition Glasses Augment China’s Railway Cops</small>](http://www.sixthtone.com/news/1001676/face-recognition-glasses-augment-chinas-railway-cops)
[<small>La Chine met en place un système de notation de ses citoyens pour 2020</small>](http://www.lefigaro.fr/secteur/high-tech/2017/12/27/32001-20171227ARTFIG00197-la-chine-met-en-place-un-systeme-de-notation-de-ses-citoyens-pour-2020.php)


## whistle blowers

![Whistle blowers](images/jnarac/surveillance/whistle_blower.jpeg)<!-- .element width="85%" -->

Note:
- cypher punk
    - libertarien
    - le renseignement est il à combattre?


## Surveillance En France

!["surveillance"](images/jnarac/surveillance/valls-prism-raccroche.jpg "surveillance")<!-- .element: width="70%" -->

Note:
- France
- 7 janvier 2015 (Je suis Charlie)
- Je suis sur écoute
- Loi sur le renseignement
  - qui abroge un cadre légal existant
    - LIL
      - le business en fera les frais
      - la démocratie aussi


## Tout avait si bien commencé

[!["SAFARI ou la chasse aux Français"](images/jnarac/surveillance/safari.jpg "SAFARI ou la chasse aux Français")](http://fr.wikipedia.org/wiki/Syst%C3%A8me_automatis%C3%A9_pour_les_fichiers_administratifs_et_le_r%C3%A9pertoire_des_individus)
[!["cnil"](images/jnarac/surveillance/cnil.jpg "cnil")](http://www.cnil.fr/)

[et puis ... sont arrivés les 4 chevaliers de l'infocalypse](http://en.wikipedia.org/wiki/Four_Horsemen_of_the_Infocalypse)

Note:
- SAFARI révélé le en 74 par Le Monde, "SAFARI ou la chasse aux Français" de Philippe Boucher
  - suite à un leak d'informaticien place Beauveau (whistlebblowers). vive opposition populaire, création des LIL, de la CNIL. Pompidou abandonne le projet.
  - notez qu'on a fait un grand bon en arrière
    - et que ce n'est pas les digital native qui vont changer la done
      - ils réfléchissent avec une vie privée semi publique depuis le début (leur parent les photos de leur échographie sur facebook)
- Fichage
  - CNIL consultatif contournable
  - intervient au niveau français ... problème
- Safe Harbor invalidé depuis le 6 octobre 2015
  - certains hébergent leur données aux USA? C'est illégal de puis 10 jours
- les 4 chevaliers de l'infocalypse: concept cypherpunk / cryptoanarchique repris par [Jacob Appelbaum](http://fr.wikipedia.org/wiki/Jacob_Appelbaum) principal hacker du projet Tor (darknet)


## Le cyberdjihadiste

!["Cyber Djihadiste"](images/jnarac/surveillance/cyberdjihadiste.jpg "Cyber Djihadiste")<!-- .element: width="95%" -->

Note:
- terrorisme : AlQaida, Aqmi, Etat Islmaique, Action direct, le terrorisme vert de greenpeace ...
  - c'est compliqué de définir le terrorisme
    - souvent une quesiton de point de vue
      - du coup c'est fluctuant
- définition du terrorisme compliquée


## Le pédonazi

!["Propagande anti pédo nazi"](images/jnarac/surveillance/jeune-pedonazis.jpg "Propagande anti pédo nazi")<!-- .element: width="35%" -->
!["Propagande anti pédo nazi"](images/jnarac/surveillance/pedonazis.jpg "Propagande anti pédo nazi")<!-- .element: width="35%" -->

Note:
- moeurs
- propos haineux
  - le web n'est pas une zone de non droit, sauf TOR
    - pose le problème du droit international
      - du contexte culturel
    - Facebook travaille bcp là dessus (contexte géo notamment)
      - bons collabos histoire de Jean Luc Lahaye qui entretenait une relation FB avec une ado de 15 ans


## Le dealer du darknet

!["dealer du darknet"](images/jnarac/surveillance/drugs.jpg "dealer du darknet")

Note:
- délinquances vente de drogues
- Silkroad vrai site dur darknet
  - réseau anonyme TOR l'Internet anonyme dans l'Internet
    - FBI clos l'aventure en 2013-2014
    - on paie en bitcoin - c'est pas visa ou master card
    - on achète aussi des contrats pour tuer des gens


## Le cyber parrain

[!["Cyber crime l'exmple Russe"](images/jnarac/surveillance/cybercrime.jpg "Cyber crime l'exmple Russe")<!-- .element width="60%" -->](http://fr.slideshare.net/Madhuranath/analyzing-the-future-of-russian-mafia)

[<small>Les activités illégales payées en cryptomonnaies représentent 72 milliards de dollars par an</small>](https://www.numerama.com/business/327828-les-activites-illegales-payees-en-cryptomonnaies-representent-72-milliards-de-dollars-par-an.html)

Note:
- exemple russe
- on peut comparer avec les benef Google
- on cherche donc a légiférer contre ces 4 fléaux
  - notez que tous existaient avant Internet
    - ils servent plus de prétexte à civiliser, brider, surveiller l'Internet
      - pourquoi surveiller?
        - parce que c'est possible
        - parce que les autres (privés et dictatures) le font
          - on sait pas quoi mais ca peut nous apprendre des trucs
          - la surveillance est inefficiente sur les sujets présentés au grand public
            - manouevre ou inculture je penche pour le second


## LPM

[!["LPM décembre 2013"](images/jnarac/surveillance/log.png "LPM décembre 2013")<!-- .element: width="110%" -->](http://ifrei.org/tiki-download_file.php?fileId=59)

Note:
- l'Etat veut jouer les Google contre l'avis de la CNIL -> et le fait
- rationaliser les côuts / temps
- C'est un faille législative, trop permissivie (cf le lien en PJ)
- LPM réelle vulnérabilité légale
    - l'accès administratif aux données de connection / fusion des dispositifs temporaires /concept flous
- le climat général se durcit
    - le novlangue: <strike>surveillance</strike> vidéoprotection LCEN Sarko 2004
    - interdiction de sites djihadiste / pédophile dans la foulée / the pirate bay même sort dès l'annonce
    - boite noire loi renseignement


## #PJLRenseignement

![Boîtes noires](images/jnarac/surveillance/boites_noires.jpg)<!-- .element style="width: 60%" -->

* [Métadonnées et DPI dans le #PLRenseignement](http://blog.jbfavre.org/2015/04/20/la-pilule-vous-la-preferez-bleue-ou-rouge/)
* [Une première « boîte noire » de la loi sur le renseignement désormais active](http://www.lemonde.fr/pixels/article/2017/11/14/les-boites-noires-de-la-loi-sur-le-renseignement-sont-desormais-actives_5214596_4408996.html)


#### « Quand on consulte des images de djihadistes, on est un djihadiste. »

![« Quand on consulte des images de djihadistes, on est un djihadiste. »](images/jnarac/surveillance/sarko.jpg "« Quand on consulte des images de djihadistes, on est un djihadiste. »")<!-- .element width="50%" -->

![« Merde je suis Emma Watson nue. »](images/jnarac/surveillance/emma-watson.png "« Merde je suis Emma Watson nue. »")<!-- .element width="50%" -->


#### Mégafichier TES

![Méga fichiers TES](images/jnarac/surveillance/megafichier_TES.jpg)<!-- .element width="25%" -->

* [<i class="fa fa-wikipedia-w" aria-hidden="true"></i> Fichage en France](https://fr.wikipedia.org/wiki/Fichage_en_France)
* [Le «fichier des gens honnêtes»](http://www.slate.fr/story/138356/saga-generalisation-fichier-des-gens-honnetes)
* [Le «mégafichier» étendu au pas de charge](http://www.liberation.fr/futurs/2017/02/21/le-megafichier-etendu-au-pas-de-charge_1549968)


### libertés nuémriques = danger ?

* partage de fichiers = piraterie?
    * [Audio Archive](https://archive.org/details/audio)
    * [Hadopi : une FAQ pour tout savoir](http://www.numerama.com/politique/129728-hadopi-faq-savoir.html)
* chiffrement = terroriste?
    * [les hackers forment les journalistes](http://owni.fr/2012/03/04/hackers-forment-journalistes/)
* Internet libre = jungle?
    * [telecomix](http://telecomix.org/)
* <i class="fa fa-btc" aria-hidden="true"></i> bitcoin = transactions illégales?
    * [dépenser ses bitcoins](https://bitcoin.fr/depenser-ses-bitcoins/)


## <i class="fa fa-ambulance" aria-hidden="true"></i> Surveillance

* [Tor](tor.html), [tails](tails.html)
* [VPN](https://fr.wikipedia.org/wiki/VPN)
  * [<i class="fa fa-github" aria-hidden="true"></i> ttlequals0/autovpn](https://github.com/ttlequals0/autovpn)
* [chiffrer](crypto.html): avec [PGP](pgp.html) ou [TLS](tls.html)
* [Bitcoin](bitcoin.html) mais surtout [Monero](https://getmonero.org/), [Zcash](https://z.cash/), ...
* Scotcher webcam, micro
* [Du maquillage pour tromper les logiciels de reconnaissance faciale](https://lexpansion.lexpress.fr/high-tech/du-maquillage-pour-tromper-les-logiciels-de-reconnaissance-faciale_1930781.html)
