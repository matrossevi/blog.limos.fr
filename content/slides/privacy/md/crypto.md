# cryptologie

# <i class="fa fa-user-secret" aria-hidden="true"></i>


## Pourquoi chiffre-t-on ?

1. Confidentialité
2. Authentification
3. Intégrité
4. Non répudiation


## Opérations cryptographiques

* Agit sur une donnée en claire
  * une chaîne caractère
  * un fichier texte
  * un fichier multimédia
  * une archive

* Appelé aussi le **clair**

* Le but de la protéger


## Hacher (to hash)

* Hash / Empreinte / Condensat
* Taille fixe (quelques octets)
* Chaque **clair** a un hash unique
  * collision = cassé
* Pas d’opération inverse
* On parle de hashage


## Hacher (to hash)

<pre><code class="hljs bash"" style="font-size: 16px"">sha256("password") = 5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</code></pre>

* [MD5]([SHA-1](https://fr.wikipedia.org/wiki/MD5) <i class="fa fa-thumbs-down"></i>
* [SHA-1](https://fr.wikipedia.org/wiki/SHA-1) <i class="fa fa-thumbs-down"></i>
* [SHA-2](https://fr.wikipedia.org/wiki/SHA-2) <i class="fa fa-thumbs-up"></i>
  * [SHA-256](https://fr.wikipedia.org/wiki/SHA-2#SHA-256)
  * [SHA-512](https://fr.wikipedia.org/wiki/SHA-2#SHA-512)
  * [SHA-384](https://fr.wikipedia.org/wiki/SHA-2#SHA-384)

* [Hash: online hash value calculator](https://www.fileformat.info/tool/hash.htm)


## Hash / attaques

* [Force brute](https://fr.wikipedia.org/wiki/Attaque_par_force_brute)
  * testé toutes les possibilités (online / offline)
* [Par dictionnaire](https://fr.wikipedia.org/wiki/Attaque_par_dictionnaire)
  * testé des combinaisons de possibilités probables (online / offline)
<!--  * [https://github.com/danielmiessler/SecLists/tree/master/Passwords](https://github.com/danielmiessler/SecLists/tree/master/Passwords) -->
* [Tables arc-en-ciel](https://fr.wikipedia.org/wiki/Rainbow_table)
  * hashés précalculés de possibilités (offline)
  * [https://crackstation.net/](https://crackstation.net/)
  * [http://project-rainbowcrack.com/table.htm](http://project-rainbowcrack.com/table.htm)
<!--* [What are the differences between dictionary attack and brute force attack?](https://security.stackexchange.com/questions/67712/what-are-the-differences-between-dictionary-attack-and-brute-force-attack#67768) -->


## Saler (to salt)

* Hasher en concaténant la donnée en claire avec une chaîne de caractères pseudo-aléatoires

* Stocker le sel au niveau de l'enregistrement de l'utilisateur
  * évite les attaques par tables arc-en-ciel
  * n'évite pas la force brute / ni les attaques par dictionnaire

* On parle de salaison


## Saler (to salt)

<pre><code class="hljs bash" style="font-size: 14px">sha256("hello"."bv5PehSMfV11Cd") = d1d3ec2e6f20fd420d50e2642992841d8338a314b8ea157c9e18477aaef226ab</code></pre>

* Utilisé notamment pour le stockage des mots de passe en base de données
  * [Salted Password Hashing - Doing it Right](https://crackstation.net/hashing-security.htm)


## Coder (to encode)

* Changer la manière de stocker la donnée en clair
  * n'est pas réellement une opération cryptographique
  * pas de secret
    * la donnée n'est pas protégée
  * on parle de codage

  <pre><code class="hljs bash" style="font-size: 25px">base64_encode("all in clear") = YWxsIGluIGNsZWFy</code></pre>

  * opération inverse décoder (to decode)

  <pre><code class="hljs bash" style="font-size: 25px">base64_decode("YWxsIGluIGNsZWFy") = all in clear</code></pre>


## Chiffrer (to encrypt)

* Rendre la compréhension de la donnée en claire impossible à quiconque ne possède pas le secret (la **clé** pour la lire)
  * on parle de chiffrement

  <pre><code class="hljs bash" style="font-size: 16px">openssl_encrypt("all in clear","aes128","secret") = d2xhqzZbLFzoCP6vNfdVOg==</code></pre>

  * opération inverse déchiffrer (to decrypt)

  <pre><code class="hljs bash" style="font-size: 16px">openssl_decrypt("d2xhqzZbLFzoCP6vNfdVOg==","aes128","secret") = all in clear</code></pre>

* Deux options : symétrique ou asymétrique


## Le reste ...

* **Décrypter**
  * chercher à deviner la donnée en claire sans disposer du secret
* **Chiffrage**
  * pour les devis (en homme / jour)
* **[Crypter, cryptage, encrypter n'existent pas](http://www.bortzmeyer.org/cryptage-n-existe-pas.html)**
  * reviendrait à tenter de chiffrer sans connaîre le secret
    * non sens


## en termes scientifiques ...

* **Cryptologie**
  * science du secret
  * **Cryptographie**
    * comment protéger les messages
  * **Cryptogranalyse**
    * comment décrypter les messages


## Chiffrement symétrique

* Dit **à clé secrète**, **à clé privée**, ou encore **à secret partagé**
    * le secret permet de chiffrer **et** de déchiffrer
    * plus le secret est **long** plus il est difficile de le deviner
    * <i class="fa fa-thumbs-o-up"></i> peu gourmand en calcul
    * <i class="fa fa-thumbs-o-down"></i> la clé doit être partagée par l'émetteur et le(s) récepteur(s)


## Chiffrements symétriques connus

* <i class="fa fa-thumbs-down"></i> faibles
  * Scytale spartiate, ROT13, Chiffre de Jules César
* <i class="fa fa-thumbs-up"></i> robustes
  *  Blowfish, AES, Twofish

![Bâton de plutarque](images/crypto/scytale.png)


## Chiffrement symétrique
### communications 2 à 2 pour n

* Une clé par destinataire
  * pour n destiantaires
    * n * (n-1) / 2 clés
    * démultiplication des clés


## Vecteur d'intialisation

* Avoir la garantie que les chiffrés sont uniques
* Se stocke en clair
  * n'est utilisable qu'avec le secret
* Deux messages identiques chiffrés avec de **vi** différents auront des chiffrés différents

```
$crypt = openssl_encrypt("all in clear", "aes128", "secret");
```
<pre><code class="hljs bash"" style="font-size: 16px">PHP Warning:  openssl_encrypt(): Using an empty Initialization Vector (iv) is potentially insecure and not recommended in /data/htdocs/blog.limos.fr/tmp.php on line 4
Warning: openssl_encrypt(): Using an empty Initialization Vector (iv) is potentially insecure and not recommended in /data/htdocs/blog.limos.fr/tmp.php on line 4</code></pre>


## Vecteur d'intialisation

### Right way

<pre><code class="hljs bash"" style="font-size: 18px">$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length("aes128"));          
$crypt = openssl_encrypt("all in clear", "aes128", "secret", true, $iv);
$clear = openssl_decrypt($crypt, "aes128", "secret", true , $iv);               
</code></pre>

note:
- https://stackoverflow.com/questions/11821195/use-of-initialization-vector-in-openssl-encrypt


## Chiffrement asymétrique

* Dit aussi à **clé publique**
  * une clé privée
  * une clé publique

* Un message chiffré avec une clé publique sera déchiffrable avec la clé privée associée
* Un message chiffré avec une clé privé sera déchiffrable avec la clé publique associée


## Chiffrement asymétrique

* La clé publique doit être connue de l'expéditeur
  * plus complexe d'un simple secret à transmettre

* Demande plus de ressources

* <i class="fa fa-thumbs-o-down"></i> Gourmand en calcul

* <i class="fa fa-thumbs-up"></i> Le plus connu
    * [RSA](https://fr.wikipedia.org/wiki/Chiffrement_RSA) since 1977
    * Algo à courbes elliptiques


## distribution des clés?

* la **clé publique** est diffusable n'importe où
  * sur un serveur de clés
  * sur une page web
  * via une carte de visite
  * via une pièce jointe
  * via un chat

* la **clé privée** est à protéger à tout prix
  * quiconque la possède peut l'utiliser en se faisant passer pour son propriétaire légitime


#### En pratique

* Alice et Bob se sont chacun générés un couple **clé publique** / **clé privée**
* Ils se sont **échangés** leur **clé publique** respective

* Alice a en sa possession
  * sa **clé publique**, sa **clé privée** et la **clé publique** de Bob

* Bob a en sa possession
  * sa **clé publique**, sa **clé privée** et la **clé publique** d'Alice


## Signature

* Alice chiffre un message avec sa clé privée et l'envoi à Bob
  * Bob peut déchiffrer le message avec la clé publique d'Alice
    * Bob est alors sûr que le message a été émis avec la clé privée d'Alice
      * **Autentification de l'origine**


#### Signature en pratique

* Alice envoie
  * le message chiffré
  * un hashé chiffré du message

* Bob peut vérifier en
  * déchiffrant le message avec la clé publique d'Alice
  * calculant le hashé du message en clair
  * déchiffrant le hashé chiffré envoyé par Alice
  * comparant le hashé calculé et le hashé déchiffré


## Signature en pratique

* Bob a pu vérifier
  * la clé à l'origine du message **autentification de l'origine**
  * l'**intégrité** du message

**autentification de l'origine** + **intégrité**

=

**non répudiation**

#### Les clés de Bob n'ont jamais été utilisées


## Chiffrement

* Bob veut maintenant écrire à Alice
  * qu'il sait être la vraie Alice
    * en tout cas celle qui possède la clé privée associée à la clé publique qu'Alice lui a donné
* Bob chiffre le message avec la clé publique d'Alice
* Bob envoie le texte chiffré
* Alice déchiffre le message grâce à sa clé privée.

* Garantie de la **confidentialité**


## Enjeu de la clé privée

* Eve a récupéré la **clé privée** de Bob (mal protégée)
  * elle peut envoyer des messages chiffrés à Alice, et à tout ceux qui font confiance à la clé publique de Bob
    * en se faisant passer pour Bob
  * elle peut lire tous les messages chiffrés avec la **clé publique** de Bob (à destination de Bob)


## Bonne pratique

* Toujours signer et chiffrer ses messages
* Toujours être certain de l'identité du possesseur de la clé
  * échange de clé IRL
* Bien protéger sa clé privée
  * stockée en sécurité
    * dans un keepass?
    * dans un coffre?
    * sur papier?
  * protégée par un mot de passe fort


## Certificat

* Clé publique
* Attributs
  * nom
  * mail
  * date d'expiration
  * algorithmes
  * ...
* Au moins une signature


## Trousseau

* Ensemble de clés
  * couples clé publique / clé privée
    * différentes fonctions
      * signature
      * chiffrement
      * création de certificat
      * authentification
  * clés publiques
    * importés (contacts)
