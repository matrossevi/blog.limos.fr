# Souveraineté

Si l'utilisateur n'utilise pas des outils qu'il contrôle ...

Il est contrôlé par ses outils


## Hacking

<table>
<tr>
<td>![RMS](images/sovereignty/stallman.jpg "RMS")<!-- .element width="60%"--></td>
<td>![ers](images/sovereignty/raymond.jpg "ers")<!-- .element width="50%" --></td>
</tr>
</table>

* Black Hat, white Hat, Grey Hat
* Ethical
  * [une brève histoire des hackers](http://www.linux-france.org/article/these/hackers_history/fr-a_brief_history_of_hackerdom_monoblock.html)
  * [code is law](http://framablog.org/2010/05/22/code-is-law-lessig/)


## logicel libre

* Concentré sur la maîtrise de l'outil par l'utilisateur
* 4 libertés fondamentales
  * la liberté d'utiliser le logiciel
  * la liberté de copier le logiciel
  * la liberté d'étudier le logiciel
  * la liberté de modifier le logiciel et de redistribuer les versions modifiées
* La viralité
  * le copie left


## Open source

* Concentré sur la façon de concevoir le code
* [La cathédrale et le bazar](https://fr.wikipedia.org/wiki/La_Cath%C3%A9drale_et_le_Bazar)

* Même Microsoft s'ouvre
  * ["An Open Letter to Hobbyists" Bill Gates, 1976](https://fr.wikipedia.org/wiki/An_Open_Letter_to_Hobbyists)
  * ["Linux is a cancer" Steve Ballmer, 2001](http://www.theregister.co.uk/2001/06/02/ballmer_linux_is_a_cancer/)
  * [Windows bientôt en Open Source](http://www.lemondeinformatique.fr/actualites/lire-windows-bientot-en-open-source-60767.html)
  * [Foire aux questions sur l'open source et l'intéropérabilité](http://www.microsoft.com/france/openness/ressources/faq.aspx)


## Logiciel privateur


## logicels libres & opensources

* OS: [Ubuntu](https://ubuntu-fr.org/)

* logiciels: [framasoft](https://framasoft.org/), [alternativeto](https://alternativeto.net/)

* Android: [F-DROID](https://f-droid.org/fr/), [<i class="fa fa-reddit" aria-hidden="true"></i> best android ROM for privacy](https://www.reddit.com/r/privacy/comments/6d3a33/best_android_rom_for_privacy/)


## logicels libres & opensources

* [Framastart](https://framastart.org/)
* [Framalibre](https://framasoft.org/rubrique2.html)
* [Framakey](https://framakey.org/Main/Index)
  * [LiberKey](https://www.liberkey.com/fr.html)
* [Framapack](https://framapack.org/)
* [AlternativeTo](http://alternativeto.net/)
* [Github](https://github.com/)
  * <strike>https://sourceforge.net/</strike>


## Services

[Degooglisons Internet](https://degooglisons-internet.org/)

* Installer ses propres services
  * sur des machines accessibles physiquement si possible
    * enjeux des connexions personnelles
* Utiliser des services décentralisés


* [OwnCloud](https://owncloud.org/) > [framadrive](https://framadrive.org/) ou [service-public.fr](https://www.service-public.fr/assistance-et-contact/aide/compte#Comment%20stocker%20vos%20documents%20) > [Google Drive](https://drive.google.com) / [Dropbox](https://www.dropbox.com) / [One Drive](https://onedrive.live.com)
* [OwnCloud](https://owncloud.org/) > [framagenda](https://framagenda.org)  > [Google calendar](https://calendar.google.com)
* [Etherpad](http://etherpad.org/) > [Framapad](https://framapad.org/) > [Google Docs](https://docs.google.com)
* [Postfix](http://www.postfix.org/) > [Proton mail](https://protonmail.com/) >  [GMail](https://mail.google.com/)
* [Qwant](https://www.qwant.com/) >  [DuckDuckGo](https://duckduckgo.com/) > [Google](https://www.google.fr)
  * g!


## Auto-hébergement

[![raspberry](images/sovereignty/raspberry.png)<!-- .element width="45%" -->](https://www.raspberrypi.org/)

* matériel open source + connexion personnelle + logiciels libres = **<3**
* [yunohost ](https://yunohost.org/#/)
* [ispconfig](https://www.ispconfig.org/)
* [La brique Internet](https://labriqueinter.net/)
