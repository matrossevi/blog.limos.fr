### X.509
* Certificat créé par la CA
* 1 seule signature : celle de la CA
* Chiffre le tuyau (TLS/SSL)
* Confiance centralisée
* Confiance distribuée
* Forêt d’arbres de confiance
### PGP
* Certificat créé par l’utilisateur
* Plusieurs signatures
* Chiffre les messages
* Graph orienté de confiance

X.509 centralise la confiance sur les CA
PGP distribue la confiance entre utilisateurs
