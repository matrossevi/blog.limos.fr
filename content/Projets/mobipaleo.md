Title: Mobipaleo
Date: 2016-11-15 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, mining


<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
        <span class="sr-only">70% complete</span>
    </div>
</div>

est un projet interdisciplinaire visant à modéliser la biodiversité à partir d'études paleoécologiques et écologiques (GEOLAB / LMGE).

Plus d'informations http://mobipaleo.univ-bpclermont.fr

Pour ce projet

* J’ai développé http://mobipaleo-ui.univ-bpclermont.fr qui permet
  * d’exploiter facilement un outil d’extraction de corrélations.
  * de présenter visuellement les résultats
  * de gérer l'accès à la plateforme via un système de gestion d'utilisateur et une interface d'administration complète

Ce travail a donnée lieu à l’écriture d’un rapport de recherche dont je suis coauteur et qui a été soumis à la conférence AKDM8 : "An Approach for Extracting Frequent (Closed) Gradual Patterns Under Temporal Constraint" http://bit.ly/2qsfSxc
