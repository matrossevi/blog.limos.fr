Title: Cepppia
Date: 2016-10-15 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
tags: www, mining


<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
        <span class="sr-only">70% complete</span>
    </div>
</div>

est un projet issu des travaux du groupe santé du Conseil de Développement du Grand Clermont. Il est porté par le CHU et co-financé par l'Union Européenne. Il propose une ambition nouvelle en santé, par la mise en œuvre du concept de médecine 4P : Prédictive, Préventive, Personnalisée et Participative. Ce projet est articulé autour d’un questionnaire sur les habitudes alimentaires et comportementales des personnes sondées et une partie analyse de données collectées permettant d’automatiser et d’enrichir la détection des profils à risque.

Plus d'informations http://bit.ly/2saexM3

Pour ce projet
  * j'ai étudié le cadre légal lié à l’hébergement et au traitement des données de santé (HDS)
  * j'ai collecté les besoins et rédigé défini une architecture technique sécurisée basée sur une analyse de risque respectant les contraintes du cahier des charges HDS.
  * j'ai rédigé les spécifications techniques pour l’appel d’offre pour un hébergement certifié HDS et participé au choix final du prestataire
  * j'encadre un ingénieur en charge du développement logiciel
  * je coordone le déploiement de l'applicatif en production avec le prestataire d'hébergement
