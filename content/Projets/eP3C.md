Title: eP3C
Date: 2017-11-21 10:27
Category: <i class='fa fa-cogs' aria-hidden='true'></i> Projets
Tags: www, mining


<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
        <span class="sr-only">30% complete</span>
    </div>
</div>


Le projet e-P3C (Pluralité des Contextes, Compétences et Comportements) est porté par Pascal Huguet, Directeur du laboratoire de psychologie sociale et cognitive (LAPSCO) de l’Université Blaise Pascal et associé au CNRS en partenariat étroit avec le Rectorat  de Clermont-Ferrand (Marie- Claude Borion Chargée de mission à la Délégation académique au Numérique et Nicolas Rocher  IPR, représentant des corps d'inspection) et deux entreprises régionales (Maskott et Perfect Memory). Ce projet, qui impliquera potentiellement 8000 élèves de l'académie dans 10 établissements scolaires est le fruit d'une réflexion concertée entre les différents partenaires avec une implication forte des corps d'inspection et des chefs d'établissements qui participeront à une grande expérimentation sur la diversification des contextes d'apprentissage. Pendant les 4 ans de son déroulement les équipes pédagogiques seront associées à la construction des protocoles de recherche. L'académie a soutenu fortement la candidature et s'engage auprès des acteurs en dégageant des heures  pour les enseignants référents dans les établissements.

L'objectif est d’optimiser l’utilisation des technologies numériques pour diversifier les contextes d’apprentissage au service de la réussite de tous les élèves. Concrètement, il s'agit de présenter un même objet d'apprentissage (par exemple un théorème, un principe de physique ou un problème de biologie) et des exercices afférents selon différentes modalités (des plus formelles aux plus ludiques) pour en augmenter la compréhension par tous les élèves, le tout au sein d'un système de tutorat intelligent (STI) capable de recommandations en fonction des actions , des erreurs et des succès de chaque élève. Les données collectées, très nombreuses, seront rigoureusement analysées pour tester l'efficacité des actions entreprises dans ce cadre. Le projet concerne tous les niveaux du collège au lycée.

Plus d'informations http://www.ac-clermont.fr/actualite/projets-e-fran-2016-09-26/?tx_ttnews%5BbackPid%5D=946

Pour ce projet

  * collecte les besoins et définition d'une architecture technique
  * participation au recrutement d’un développeur pour la conception de la plateforme
  * encadrement de deux développeurs
  * coordination avec
      * le rectorat de Clermont-Ferrand
      * Maskott pour la récupération des données en provenance du STI
      * les chercheurs en sciences sociales et cognitives pour la mise en place d'es questionnaires
