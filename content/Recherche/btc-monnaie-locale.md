Title: Bitcoin et les monnaies locales
Date: 2010-05-17 10:27
Category: <i class='fa fa-flask' aria-hidden='true'></i> Recherche


Passionné par les cryptomonnaies, je suis à l’origine d’une collaboration entre une chercheuse en économie, spécialisée dans l’économie sociale et solidaire, et un chercheur en sécurité du LIMOS dont bitcoin est un des sujets de recherche.

Pour ce projet

* j'ai analysé l'implémentation de 2 monnaies virtuelles basées sur la technologie blockchain openUDC (http://www.openudc.org/) et duniter (https://duniter.org/).

Cette collaboration a débouché sur l’écriture d’un rapport de recherche "Les monnaies virtuelles sont-elles des outils d'avenir?" (halshs-01467329v1) dont je suis co-auteur et qui est appelé à être publié dans le magazine Usebek & Rica.
